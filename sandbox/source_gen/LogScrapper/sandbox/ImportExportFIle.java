package LogScrapper.sandbox;

/*Generated by MPS */

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.LinkedList;

public class ImportExportFIle {
  public static void main(String[] args) {
    System.out.println("Starting log anlysis " + "ImportExportFIle");
    File logFile = new File("E:\\cygwin\\home\\Ganesh\\barclaysretail\\05062015\\mirevnue.log");
    try {
      BufferedReader logReader = new BufferedReader(new FileReader(logFile));
      HashMap<String, WorkchunkTaskIdAssociation> map_WorkchunkTaskIdAssociation = new HashMap<String, WorkchunkTaskIdAssociation>();
      while (logReader.ready()) {
        String line = logReader.readLine();
        WorkchunkTaskIdAssociation WorkchunkTaskIdAssociation_item = WorkchunkTaskIdAssociation.parse(line);
        if (null != WorkchunkTaskIdAssociation_item) {
          map_WorkchunkTaskIdAssociation.put(WorkchunkTaskIdAssociation_item.getWorkchunkId(), WorkchunkTaskIdAssociation_item);
        }
      }
      System.out.println("recorded items are " + map_WorkchunkTaskIdAssociation.size());
      exporttoFile_a(map_WorkchunkTaskIdAssociation);

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public static void exporttoFile_a(HashMap workchunks) {
    List names = new LinkedList();
    names.add("captureDateTime");
    names.add("threadIdPrefix");
    names.add("threadId");
    names.add("workChunkIdPrefix");
    names.add("workchunkId");
    names.add("workListIdFromPrefix");
    names.add("worklistFromId");
    names.add("worklistIdToIdPrefix");
    names.add("worklistIdTo");
    names.add("taskIdPrefix");
    names.add("taskId");
    names.add("groupingKeyPrefix");
    names.add("worklistStatusPrefix");
    names.add("workchunkStatus");
    names.add("chunkSizePrefix");
    names.add("chunkSize");
    names.add("completedCountPrefix");
    names.add("completedCount");
    names.add("errorCountPrefix");
    names.add("errorCount");
    names.add("timeTakenPrefix");
    names.add("timeTaken");
    names.add("postFix");
    String fileName = "C:\\Users\\Ganesh\\Desktop\\shoorting.csv";
    String colSep = ">>";
    WorkchunkTaskIdAssociation.exportToCSV(workchunks, fileName, colSep, names);
  }
}
