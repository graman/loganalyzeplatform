<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:ed0f267f-5afe-41a5-bd66-db44f04c61c5(LogScrapper.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core" version="-1" />
    <use id="ecee5794-6eda-4903-8303-84b2f078edf4" name="LogScrapper" version="0" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="-1" />
    <use id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc" version="-1" />
  </languages>
  <imports>
    <import index="l5h2" ref="f2d18ae0-3a8e-433b-9851-e50a45f80e9b/f:java_stub#f2d18ae0-3a8e-433b-9851-e50a45f80e9b#org.apache.poi.ss.usermodel(org.apache.poi/org.apache.poi.ss.usermodel@java_stub)" />
    <import index="3dck" ref="f2d18ae0-3a8e-433b-9851-e50a45f80e9b/f:java_stub#f2d18ae0-3a8e-433b-9851-e50a45f80e9b#org.apache.poi.xssf(org.apache.poi/org.apache.poi.xssf@java_stub)" />
    <import index="e2lb" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.lang(JDK/java.lang@java_stub)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="4269842503726207156" name="jetbrains.mps.baseLanguage.structure.LongLiteral" flags="nn" index="1adDum">
        <property id="4269842503726207157" name="value" index="1adDun" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="ecee5794-6eda-4903-8303-84b2f078edf4" name="LogScrapper">
      <concept id="2297730488494069886" name="LogScrapper.structure.Pattern" flags="ng" index="2bDjna">
        <child id="2297730488494069914" name="expressions" index="2bDjkI" />
        <child id="2297730488495588146" name="line" index="2bJAU6" />
        <child id="679749503403348600" name="identifier" index="1JX2rE" />
      </concept>
      <concept id="3486839572890599865" name="LogScrapper.structure.LoggedItemReference" flags="ng" index="yyfYB">
        <reference id="3486839572890599893" name="logItem" index="yyfZb" />
      </concept>
      <concept id="8888036694238729681" name="LogScrapper.structure.Exporter" flags="ng" index="HT3ep">
        <reference id="679749503403965157" name="logItem" index="1JMGTR" />
      </concept>
      <concept id="1592023109706793268" name="LogScrapper.structure.LogFilSelector" flags="ng" index="1ekk6x">
        <property id="3486839572890627770" name="fileLocation" index="yy12$" />
        <property id="8888036694244569665" name="fileNme" index="Gjhg9" />
        <child id="3486839572890590066" name="loggedItems" index="yyalG" />
        <child id="679749503403971046" name="exporter" index="1JMElO" />
      </concept>
      <concept id="7034005416250508705" name="LogScrapper.structure.LoggedItem" flags="ig" index="3AlcG9">
        <child id="2297730488494076868" name="pattern" index="2bDhTK" />
      </concept>
      <concept id="7034005416250608564" name="LogScrapper.structure.Member" flags="ig" index="3AlOks">
        <property id="2297730488494150658" name="pattern" index="2bD7AQ" />
        <property id="2297730488494027483" name="index" index="2bD_XJ" />
      </concept>
      <concept id="679749503403965090" name="LogScrapper.structure.CSVExport" flags="ng" index="1JMGSK">
        <property id="8888036694244338956" name="fileNme" index="GiCH4" />
        <property id="8888036694243495091" name="exportAll" index="GnuFV" />
        <property id="679749503404374123" name="fileName" index="1JL8NT" />
        <property id="679749503405737752" name="separator" index="1JOrea" />
      </concept>
      <concept id="679749503403162673" name="LogScrapper.structure.ItemIdentifier" flags="ng" index="1JYgyz">
        <reference id="679749503403297546" name="item" index="1JXfYo" />
      </concept>
    </language>
  </registry>
  <node concept="1ekk6x" id="1oo09lojgAA">
    <property role="TrG5h" value="ImportExportFIle" />
    <property role="yy12$" value="E:\cygwin\home\Ganesh\barclaysretail\05062015\mirevnue.log" />
    <property role="Gjhg9" value="mirevnue.log" />
    <node concept="1JMGSK" id="4Iz7iG40cWw" role="1JMElO">
      <property role="1JL8NT" value="C:\Users\Ganesh\Desktop\shoorting.csv" />
      <property role="GiCH4" value="shoorting.csv" />
      <property role="1JOrea" value="&gt;&gt;" />
      <property role="GnuFV" value="true" />
      <ref role="1JMGTR" node="74LOPGMd05X" resolve="WorkchunkTaskIdAssociation" />
    </node>
    <node concept="yyfYB" id="4Iz7iG41vP0" role="yyalG">
      <ref role="yyfZb" node="74LOPGMd05X" resolve="WorkchunkTaskIdAssociation" />
    </node>
  </node>
  <node concept="312cEu" id="3SMMV5ZScpF">
    <property role="TrG5h" value="Test" />
    <node concept="312cEg" id="3SMMV5ZScqy" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="ace" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="3SMMV5ZScpY" role="1B3o_S" />
      <node concept="3uibUv" id="4Iz7iG3H8lk" role="1tU5fm">
        <ref role="3uigEE" to="e2lb:~String" resolve="String" />
      </node>
      <node concept="Xl_RD" id="3SMMV5ZScr4" role="33vP2m">
        <property role="Xl_RC" value="adsd" />
      </node>
    </node>
    <node concept="312cEg" id="3SMMV5ZScrU" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="len" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm6S6" id="3SMMV5ZScrB" role="1B3o_S" />
      <node concept="2OqwBi" id="1By676Os0O1" role="33vP2m">
        <node concept="37vLTw" id="1By676OrRwb" role="2Oq$k0">
          <ref role="3cqZAo" node="3SMMV5ZScqy" resolve="ace" />
        </node>
        <node concept="liA8E" id="1By676Os0QZ" role="2OqNvi">
          <ref role="37wK5l" to="e2lb:~String.length():int" resolve="length" />
        </node>
      </node>
      <node concept="3uibUv" id="4Iz7iG3NIHi" role="1tU5fm">
        <ref role="3uigEE" to="e2lb:~Integer" resolve="Integer" />
      </node>
    </node>
    <node concept="3clFb_" id="1By676OdXIQ" role="jymVt">
      <property role="TrG5h" value="asa" />
      <node concept="3cqZAl" id="1By676OdXIS" role="3clF45" />
      <node concept="3Tm1VV" id="1By676OdXIT" role="1B3o_S" />
      <node concept="3clFbS" id="1By676OdXIU" role="3clF47">
        <node concept="3SKdUt" id="4Iz7iG3UCpy" role="3cqZAp">
          <node concept="3SKdUq" id="4Iz7iG3UCsw" role="3SKWNk">
            <property role="3SKdUp" value="sdsd" />
          </node>
        </node>
        <node concept="3clFbF" id="3FXpqMT4b_M" role="3cqZAp">
          <node concept="2OqwBi" id="3FXpqMT4bAz" role="3clFbG">
            <node concept="37vLTw" id="3FXpqMT4b_L" role="2Oq$k0">
              <ref role="3cqZAo" node="3SMMV5ZScqy" resolve="ace" />
            </node>
            <node concept="liA8E" id="3FXpqMT4bDA" role="2OqNvi">
              <ref role="37wK5l" to="e2lb:~String.toString():java.lang.String" resolve="toString" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="74LOPGM5lbz" role="3cqZAp">
          <node concept="2OqwBi" id="74LOPGM5leF" role="3clFbG">
            <node concept="Xl_RD" id="74LOPGM5lby" role="2Oq$k0">
              <property role="Xl_RC" value="Sdsd" />
            </node>
            <node concept="liA8E" id="74LOPGM5ljj" role="2OqNvi">
              <ref role="37wK5l" to="e2lb:~String.toString():java.lang.String" resolve="toString" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3clFb_" id="4Iz7iG3JZgQ" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="converForStr" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="4Iz7iG3JZgT" role="3clF47">
        <node concept="3cpWs6" id="4Iz7iG3KMGD" role="3cqZAp">
          <node concept="1adDum" id="4Iz7iG3KMEB" role="3cqZAk">
            <property role="1adDun" value="2L" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="4Iz7iG3JZcq" role="1B3o_S" />
      <node concept="3uibUv" id="4Iz7iG3JZij" role="3clF45">
        <ref role="3uigEE" to="e2lb:~Long" resolve="Long" />
      </node>
      <node concept="37vLTG" id="4Iz7iG3JZqT" role="3clF46">
        <property role="TrG5h" value="ace" />
        <node concept="3uibUv" id="4Iz7iG3JZqS" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="3SMMV5ZScpG" role="1B3o_S" />
  </node>
  <node concept="3AlcG9" id="74LOPGMd05X">
    <property role="TrG5h" value="WorkchunkTaskIdAssociation" />
    <node concept="3Tm1VV" id="74LOPGMd05Y" role="1B3o_S" />
    <node concept="2bDjna" id="74LOPGMd05Z" role="2bDhTK">
      <node concept="Xl_RD" id="74LOPGMd09j" role="2bJAU6">
        <property role="Xl_RC" value="" />
      </node>
      <node concept="3AlOks" id="74LOPGMd09x" role="2bDjkI">
        <property role="2bD_XJ" value="1" />
        <property role="2bD7AQ" value=".*" />
        <property role="TrG5h" value="captureDateTime" />
        <property role="3TUv4t" value="false" />
        <node concept="3Tm6S6" id="74LOPGMd09z" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd09K" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd09P" role="2bDjkI">
        <property role="2bD_XJ" value="2" />
        <property role="2bD7AQ" value=" \\| MIREV \\| INFO \\| LOG \\| BatchWorkerThread \\| RUNTIME \\|  \\| " />
        <property role="TrG5h" value="threadIdPrefix" />
        <node concept="3Tm6S6" id="74LOPGMd09R" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0a7" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0aK" role="2bDjkI">
        <property role="2bD_XJ" value="3" />
        <property role="2bD7AQ" value=".*" />
        <property role="TrG5h" value="threadId" />
        <node concept="3Tm6S6" id="74LOPGMd0aM" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0b3" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0bK" role="2bDjkI">
        <property role="2bD_XJ" value="4" />
        <property role="2bD7AQ" value=" : Processing WorkChunk\\[id:" />
        <property role="TrG5h" value="workChunkIdPrefix" />
        <node concept="3Tm6S6" id="74LOPGMd0bM" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0ce" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0cZ" role="2bDjkI">
        <property role="2bD_XJ" value="5" />
        <property role="2bD7AQ" value=".*" />
        <property role="TrG5h" value="workchunkId" />
        <node concept="3Tm6S6" id="74LOPGMd0d1" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0du" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0ej" role="2bDjkI">
        <property role="2bD_XJ" value="6" />
        <property role="2bD7AQ" value=",fromId:" />
        <property role="TrG5h" value="workListIdFromPrefix" />
        <node concept="3Tm6S6" id="74LOPGMd0el" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0eZ" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0fS" role="2bDjkI">
        <property role="2bD_XJ" value="7" />
        <property role="2bD7AQ" value=".*" />
        <property role="TrG5h" value="worklistFromId" />
        <node concept="3Tm6S6" id="74LOPGMd0fU" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0gw" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0ht" role="2bDjkI">
        <property role="2bD_XJ" value="8" />
        <property role="2bD7AQ" value=",toId:" />
        <property role="TrG5h" value="worklistIdToIdPrefix" />
        <node concept="3Tm6S6" id="74LOPGMd0hv" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0ic" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0jd" role="2bDjkI">
        <property role="2bD_XJ" value="9" />
        <property role="2bD7AQ" value=".*" />
        <property role="TrG5h" value="worklistIdTo" />
        <node concept="3Tm6S6" id="74LOPGMd0jf" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0jV" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0l0" role="2bDjkI">
        <property role="2bD_XJ" value="10" />
        <property role="2bD7AQ" value=",taskId:" />
        <property role="TrG5h" value="taskIdPrefix" />
        <node concept="3Tm6S6" id="74LOPGMd0l2" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0lN" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0mW" role="2bDjkI">
        <property role="2bD_XJ" value="11" />
        <property role="2bD7AQ" value=".*" />
        <property role="TrG5h" value="taskId" />
        <node concept="3Tm6S6" id="74LOPGMd0mY" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0nK" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0oX" role="2bDjkI">
        <property role="2bD_XJ" value="12" />
        <property role="2bD7AQ" value=",groupingKey:" />
        <property role="TrG5h" value="groupingKeyPrefix" />
        <node concept="3Tm6S6" id="74LOPGMd0oZ" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0pQ" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0r7" role="2bDjkI">
        <property role="2bD_XJ" value="13" />
        <property role="2bD7AQ" value="null,status:" />
        <property role="TrG5h" value="worklistStatusPrefix" />
        <node concept="3Tm6S6" id="74LOPGMd0r9" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0sb" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0tw" role="2bDjkI">
        <property role="2bD_XJ" value="14" />
        <property role="2bD7AQ" value=".*" />
        <property role="TrG5h" value="workchunkStatus" />
        <node concept="3Tm6S6" id="74LOPGMd0ty" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0vB" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0x0" role="2bDjkI">
        <property role="2bD_XJ" value="15" />
        <property role="2bD7AQ" value=",chunkSize:" />
        <property role="TrG5h" value="chunkSizePrefix" />
        <node concept="3Tm6S6" id="74LOPGMd0x2" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0y4" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0zx" role="2bDjkI">
        <property role="2bD_XJ" value="16" />
        <property role="2bD7AQ" value=".*" />
        <property role="TrG5h" value="chunkSize" />
        <node concept="3Tm6S6" id="74LOPGMd0zz" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0$B" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0A8" role="2bDjkI">
        <property role="2bD_XJ" value="17" />
        <property role="2bD7AQ" value=",completedCount:" />
        <property role="TrG5h" value="completedCountPrefix" />
        <node concept="3Tm6S6" id="74LOPGMd0Aa" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0B8" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0Fb" role="2bDjkI">
        <property role="2bD_XJ" value="18" />
        <property role="2bD7AQ" value=".*" />
        <property role="TrG5h" value="completedCount" />
        <node concept="3Tm6S6" id="74LOPGMd0Fd" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0Gr" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0I4" role="2bDjkI">
        <property role="2bD_XJ" value="19" />
        <property role="2bD7AQ" value=",errorCount:" />
        <property role="TrG5h" value="errorCountPrefix" />
        <node concept="3Tm6S6" id="74LOPGMd0I6" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0Js" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0L9" role="2bDjkI">
        <property role="2bD_XJ" value="20" />
        <property role="2bD7AQ" value=".*" />
        <property role="TrG5h" value="errorCount" />
        <node concept="3Tm6S6" id="74LOPGMd0Lb" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0My" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0Oj" role="2bDjkI">
        <property role="2bD_XJ" value="21" />
        <property role="2bD7AQ" value=",timeTaken:" />
        <property role="TrG5h" value="timeTakenPrefix" />
        <node concept="3Tm6S6" id="74LOPGMd0Ol" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0PQ" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0RF" role="2bDjkI">
        <property role="2bD_XJ" value="22" />
        <property role="2bD7AQ" value=".*" />
        <property role="TrG5h" value="timeTaken" />
        <node concept="3Tm6S6" id="74LOPGMd0RH" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0SU" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="3AlOks" id="74LOPGMd0UN" role="2bDjkI">
        <property role="2bD_XJ" value="23" />
        <property role="2bD7AQ" value="\\]" />
        <property role="TrG5h" value="postFix" />
        <node concept="3Tm6S6" id="74LOPGMd0UP" role="1B3o_S" />
        <node concept="3uibUv" id="74LOPGMd0Wn" role="1tU5fm">
          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
        </node>
      </node>
      <node concept="1JYgyz" id="4Iz7iG40b3V" role="1JX2rE">
        <ref role="1JXfYo" node="74LOPGMd0cZ" resolve="workchunkId" />
      </node>
    </node>
  </node>
</model>

