<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:1191a615-0dc2-4cce-b4a2-800ec40bd400(LogScrapper.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="80av" ref="r:304512bc-2ab8-4538-a01c-29a28f4fb5a1(LogScrapper.structure)" />
    <import index="e2lb" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.lang(JDK/java.lang@java_stub)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" />
    <import index="tpek" ref="r:00000000-0000-4000-0000-011c895902c0(jetbrains.mps.baseLanguage.behavior)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194628440" name="jetbrains.mps.lang.behavior.structure.SuperNodeExpression" flags="nn" index="13iAh5" />
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="1196350785113" name="jetbrains.mps.lang.quotation.structure.Quotation" flags="nn" index="2c44tf">
        <child id="1196350785114" name="quotedNode" index="2c44tc" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="1167227138527" name="jetbrains.mps.baseLanguage.logging.structure.LogStatement" flags="nn" index="34ab3g">
        <property id="1167245565795" name="severity" index="35gtTG" />
        <child id="1167227463056" name="logExpression" index="34bqiv" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1145383075378" name="jetbrains.mps.lang.smodel.structure.SNodeListType" flags="in" index="2I9FWS">
        <reference id="1145383142433" name="elementConcept" index="2I9WkF" />
      </concept>
      <concept id="1227264722563" name="jetbrains.mps.lang.smodel.structure.EqualsStructurallyExpression" flags="nn" index="2YFouu" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
    </language>
  </registry>
  <node concept="13h7C7" id="1ZzbrikgeiU">
    <ref role="13h7C2" to="80av:66tNEy7MCIO" resolve="Member" />
    <node concept="13i0hz" id="1ZzbrikgekP" role="13h7CS">
      <property role="TrG5h" value="convertToCamelCase" />
      <property role="DiZV1" value="true" />
      <property role="od$2w" value="false" />
      <node concept="3clFbS" id="1Zzbrikgby8" role="3clF47">
        <node concept="3cpWs8" id="1Zzbrikgbyb" role="3cqZAp">
          <node concept="3cpWsn" id="1Zzbrikgbyc" role="3cpWs9">
            <property role="TrG5h" value="firstLetter" />
            <node concept="17QB3L" id="1ZzbrikgeMr" role="1tU5fm" />
            <node concept="2OqwBi" id="1Zzbrikgbye" role="33vP2m">
              <node concept="2OqwBi" id="1Zzbrikgbyf" role="2Oq$k0">
                <node concept="2OqwBi" id="1Zzbrikgbyg" role="2Oq$k0">
                  <node concept="13iPFW" id="1ZzbrikgeyG" role="2Oq$k0" />
                  <node concept="3TrcHB" id="1Zzbrikgbyi" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
                <node concept="liA8E" id="1Zzbrikgbyj" role="2OqNvi">
                  <ref role="37wK5l" to="e2lb:~String.substring(int,int):java.lang.String" resolve="substring" />
                  <node concept="3cmrfG" id="1Zzbrikgbyk" role="37wK5m">
                    <property role="3cmrfH" value="0" />
                  </node>
                  <node concept="3cmrfG" id="1Zzbrikgbyl" role="37wK5m">
                    <property role="3cmrfH" value="1" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="1Zzbrikgbym" role="2OqNvi">
                <ref role="37wK5l" to="e2lb:~String.toUpperCase():java.lang.String" resolve="toUpperCase" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="1Zzbrikgbyn" role="3cqZAp">
          <node concept="3cpWsn" id="1Zzbrikgbyo" role="3cpWs9">
            <property role="TrG5h" value="restOfName" />
            <node concept="17QB3L" id="1ZzbrikgeRu" role="1tU5fm" />
            <node concept="2OqwBi" id="1Zzbrikgbyq" role="33vP2m">
              <node concept="2OqwBi" id="1Zzbrikgbyr" role="2Oq$k0">
                <node concept="13iPFW" id="1ZzbrikgeBF" role="2Oq$k0" />
                <node concept="3TrcHB" id="1Zzbrikgbyt" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
              <node concept="liA8E" id="1Zzbrikgbyu" role="2OqNvi">
                <ref role="37wK5l" to="e2lb:~String.substring(int):java.lang.String" resolve="substring" />
                <node concept="3cmrfG" id="1Zzbrikgbyv" role="37wK5m">
                  <property role="3cmrfH" value="1" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1ZzbrikgbyA" role="3cqZAp">
          <node concept="3cpWs3" id="1Zzbrikgbyz" role="3cqZAk">
            <node concept="37vLTw" id="1Zzbrikgby$" role="3uHU7w">
              <ref role="3cqZAo" node="1Zzbrikgbyo" resolve="restOfName" />
            </node>
            <node concept="37vLTw" id="1Zzbrikgby_" role="3uHU7B">
              <ref role="3cqZAo" node="1Zzbrikgbyc" resolve="firstLetter" />
            </node>
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="1ZzbrikgeHd" role="3clF45" />
      <node concept="3Tm1VV" id="1ZzbrikgeX0" role="1B3o_S" />
    </node>
    <node concept="13i0hz" id="4Iz7iG3GNk4" role="13h7CS">
      <property role="TrG5h" value="isStringType" />
      <node concept="3Tm1VV" id="4Iz7iG3GNk5" role="1B3o_S" />
      <node concept="10P_77" id="4Iz7iG3GNyb" role="3clF45" />
      <node concept="3clFbS" id="4Iz7iG3GNk7" role="3clF47">
        <node concept="3clFbJ" id="4Iz7iG3GN$_" role="3cqZAp">
          <node concept="3clFbS" id="4Iz7iG3GN$A" role="3clFbx">
            <node concept="3clFbJ" id="4Iz7iG3GSDa" role="3cqZAp">
              <node concept="3clFbS" id="4Iz7iG3GSDb" role="3clFbx">
                <node concept="3cpWs8" id="4Iz7iG3If8T" role="3cqZAp">
                  <node concept="3cpWsn" id="4Iz7iG3If8U" role="3cpWs9">
                    <property role="TrG5h" value="result" />
                    <node concept="10P_77" id="4Iz7iG3If8N" role="1tU5fm" />
                    <node concept="2YFouu" id="4Iz7iG3MR8i" role="33vP2m">
                      <node concept="2OqwBi" id="4Iz7iG3If8W" role="3uHU7B">
                        <node concept="2OqwBi" id="4Iz7iG3If8Y" role="2Oq$k0">
                          <node concept="13iPFW" id="4Iz7iG3If8Z" role="2Oq$k0" />
                          <node concept="3TrEf2" id="4Iz7iG3If90" role="2OqNvi">
                            <ref role="3Tt5mk" to="tpee:4VkOLwjf83e" />
                          </node>
                        </node>
                        <node concept="2qgKlT" id="4Iz7iG3If91" role="2OqNvi">
                          <ref role="37wK5l" to="tpek:hEwIzO1" resolve="getJavaType" />
                        </node>
                      </node>
                      <node concept="2c44tf" id="4Iz7iG3If92" role="3uHU7w">
                        <node concept="3uibUv" id="4Iz7iG3If93" role="2c44tc">
                          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs6" id="4Iz7iG3Ifxr" role="3cqZAp">
                  <node concept="37vLTw" id="4Iz7iG3If94" role="3cqZAk">
                    <ref role="3cqZAo" node="4Iz7iG3If8U" resolve="result" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="4Iz7iG3GUI3" role="3clFbw">
                <node concept="2OqwBi" id="4Iz7iG3GSMG" role="2Oq$k0">
                  <node concept="13iPFW" id="4Iz7iG3GSDo" role="2Oq$k0" />
                  <node concept="3TrEf2" id="4Iz7iG3GU2h" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:4VkOLwjf83e" />
                  </node>
                </node>
                <node concept="3x8VRR" id="4Iz7iG3GV5k" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="3clFbT" id="4Iz7iG3OVkp" role="3clFbw">
            <property role="3clFbU" value="true" />
          </node>
        </node>
        <node concept="3cpWs6" id="4Iz7iG3GQfP" role="3cqZAp">
          <node concept="3clFbT" id="4Iz7iG3GQgr" role="3cqZAk">
            <property role="3clFbU" value="false" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="1ZzbrikgeiV" role="13h7CW">
      <node concept="3clFbS" id="1ZzbrikgeiW" role="2VODD2">
        <node concept="3clFbF" id="3SMMV5ZRCRQ" role="3cqZAp">
          <node concept="37vLTI" id="3SMMV5ZRHHE" role="3clFbG">
            <node concept="2ShNRf" id="3SMMV5ZRHKc" role="37vLTx">
              <node concept="3zrR0B" id="3SMMV5ZRKiF" role="2ShVmc">
                <node concept="3Tqbb2" id="3SMMV5ZRKiH" role="3zrR0E">
                  <ref role="ehGHo" to="tpee:gFTm6Wc" resolve="PrivateVisibility" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="3SMMV5ZRDjK" role="37vLTJ">
              <node concept="13iPFW" id="3SMMV5ZRCRP" role="2Oq$k0" />
              <node concept="3TrEf2" id="3SMMV5ZRH1G" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:h9B3oxE" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="74LOPGM396P" role="3cqZAp">
          <node concept="37vLTI" id="74LOPGM3b1D" role="3clFbG">
            <node concept="3cmrfG" id="74LOPGM3b8L" role="37vLTx">
              <property role="3cmrfH" value="0" />
            </node>
            <node concept="2OqwBi" id="74LOPGM39eX" role="37vLTJ">
              <node concept="13iPFW" id="74LOPGM396N" role="2Oq$k0" />
              <node concept="3TrcHB" id="74LOPGM39QQ" role="2OqNvi">
                <ref role="3TsBF5" to="80av:1Zzbrikhnbr" resolve="index" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4Iz7iG3OV_A" role="3cqZAp">
          <node concept="37vLTI" id="4Iz7iG3OX4P" role="3clFbG">
            <node concept="2c44tf" id="4Iz7iG3OXau" role="37vLTx">
              <node concept="3uibUv" id="4Iz7iG3OXfp" role="2c44tc">
                <ref role="3uigEE" to="e2lb:~String" resolve="String" />
              </node>
            </node>
            <node concept="2OqwBi" id="4Iz7iG3OVJZ" role="37vLTJ">
              <node concept="13iPFW" id="4Iz7iG3OV_$" role="2Oq$k0" />
              <node concept="3TrEf2" id="4Iz7iG3OWnS" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:4VkOLwjf83e" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="3SMMV5ZR9Fs">
    <ref role="13h7C2" to="80av:1ZzbrikhxxY" resolve="Pattern" />
    <node concept="13hLZK" id="3SMMV5ZR9Ft" role="13h7CW">
      <node concept="3clFbS" id="3SMMV5ZR9Fu" role="2VODD2" />
    </node>
  </node>
  <node concept="13h7C7" id="3SMMV5ZZUAf">
    <ref role="13h7C2" to="80av:66tNEy7Mgmx" resolve="LoggedItem" />
    <node concept="13hLZK" id="3SMMV5ZZUAg" role="13h7CW">
      <node concept="3clFbS" id="3SMMV5ZZUAh" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="74LOPGMcXtf" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="TrG5h" value="isStatic" />
      <property role="13i0it" value="false" />
      <ref role="13i0hy" to="tpek:6r77ob2USS8" resolve="isStatic" />
      <node concept="3Tm1VV" id="74LOPGMcXtg" role="1B3o_S" />
      <node concept="3clFbS" id="74LOPGMcXtE" role="3clF47">
        <node concept="3clFbF" id="74LOPGMcXtJ" role="3cqZAp">
          <node concept="3clFbT" id="74LOPGMcXzj" role="3clFbG">
            <property role="3clFbU" value="false" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="74LOPGMcXtF" role="3clF45" />
    </node>
    <node concept="13i0hz" id="74LOPGMcXAH" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="TrG5h" value="getExtendedClassifierTypes" />
      <property role="13i0it" value="false" />
      <ref role="13i0hy" to="tpek:1UeCwxlWKny" resolve="getExtendedClassifierTypes" />
      <node concept="3Tm1VV" id="74LOPGMcXAI" role="1B3o_S" />
      <node concept="3clFbS" id="74LOPGMcXBB" role="3clF47">
        <node concept="3clFbF" id="74LOPGMcXBG" role="3cqZAp">
          <node concept="2OqwBi" id="74LOPGMcXBD" role="3clFbG">
            <node concept="13iAh5" id="74LOPGMcXBE" role="2Oq$k0" />
            <node concept="2qgKlT" id="74LOPGMcXBF" role="2OqNvi">
              <ref role="37wK5l" to="tpek:1UeCwxlWKny" resolve="getExtendedClassifierTypes" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2I9FWS" id="74LOPGMcXBC" role="3clF45">
        <ref role="2I9WkF" to="tpee:g7uibYu" resolve="ClassifierType" />
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="74LOPGM7dGZ">
    <ref role="13h7C2" to="80av:74LOPGM6KOt" resolve="instanceMethodDeclarationEx" />
    <node concept="13hLZK" id="74LOPGM7dH0" role="13h7CW">
      <node concept="3clFbS" id="74LOPGM7dH1" role="2VODD2">
        <node concept="3clFbF" id="74LOPGM7dIC" role="3cqZAp">
          <node concept="37vLTI" id="74LOPGM7gvm" role="3clFbG">
            <node concept="2OqwBi" id="74LOPGM7dS$" role="37vLTJ">
              <node concept="13iPFW" id="74LOPGM7dIB" role="2Oq$k0" />
              <node concept="3TrcHB" id="74LOPGM7fwl" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
            <node concept="Xl_RD" id="74LOPGM7gJa" role="37vLTx">
              <property role="Xl_RC" value="postInitTransformers" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="74LOPGM7gWA" role="3cqZAp">
          <node concept="37vLTI" id="74LOPGM7j$N" role="3clFbG">
            <node concept="2OqwBi" id="74LOPGM7h6T" role="37vLTJ">
              <node concept="13iPFW" id="74LOPGM7gW$" role="2Oq$k0" />
              <node concept="3TrEf2" id="74LOPGM7iIE" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:h9B3oxE" />
              </node>
            </node>
            <node concept="2ShNRf" id="74LOPGM7j_L" role="37vLTx">
              <node concept="3zrR0B" id="74LOPGM7j_M" role="2ShVmc">
                <node concept="3Tqbb2" id="74LOPGM7j_N" role="3zrR0E">
                  <ref role="ehGHo" to="tpee:gFTm1ZL" resolve="PublicVisibility" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="74LOPGM7jJP" role="3cqZAp">
          <node concept="37vLTI" id="74LOPGM7lM1" role="3clFbG">
            <node concept="2OqwBi" id="74LOPGM7jUI" role="37vLTJ">
              <node concept="13iPFW" id="74LOPGM7jJN" role="2Oq$k0" />
              <node concept="3TrEf2" id="74LOPGM7kIK" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:fzclF7X" />
              </node>
            </node>
            <node concept="2ShNRf" id="74LOPGM7lMF" role="37vLTx">
              <node concept="3zrR0B" id="74LOPGM7lMG" role="2ShVmc">
                <node concept="3Tqbb2" id="74LOPGM7lMH" role="3zrR0E">
                  <ref role="ehGHo" to="tpee:fzcqZ_H" resolve="VoidType" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="4Iz7iG3JZai">
    <ref role="13h7C2" to="80av:4Iz7iG3JYy5" resolve="instanceMethodDeclarationExReference" />
    <node concept="13hLZK" id="4Iz7iG3JZaj" role="13h7CW">
      <node concept="3clFbS" id="4Iz7iG3JZak" role="2VODD2">
        <node concept="3cpWs8" id="4Iz7iG3K0B4" role="3cqZAp">
          <node concept="3cpWsn" id="4Iz7iG3K0B7" role="3cpWs9">
            <property role="TrG5h" value="stringNode" />
            <node concept="3Tqbb2" id="4Iz7iG3K0B3" role="1tU5fm">
              <ref role="ehGHo" to="tpee:fz7vLUk" resolve="ParameterDeclaration" />
            </node>
            <node concept="2ShNRf" id="4Iz7iG3K0BC" role="33vP2m">
              <node concept="3zrR0B" id="4Iz7iG3K0BA" role="2ShVmc">
                <node concept="3Tqbb2" id="4Iz7iG3K0BB" role="3zrR0E">
                  <ref role="ehGHo" to="tpee:fz7vLUk" resolve="ParameterDeclaration" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4Iz7iG3K0BZ" role="3cqZAp">
          <node concept="37vLTI" id="4Iz7iG3K1J7" role="3clFbG">
            <node concept="Xl_RD" id="4Iz7iG3K1Jw" role="37vLTx">
              <property role="Xl_RC" value="input" />
            </node>
            <node concept="2OqwBi" id="4Iz7iG3K0H7" role="37vLTJ">
              <node concept="37vLTw" id="4Iz7iG3K0BX" role="2Oq$k0">
                <ref role="3cqZAo" node="4Iz7iG3K0B7" resolve="stringNode" />
              </node>
              <node concept="3TrcHB" id="4Iz7iG3K187" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4Iz7iG3K1MO" role="3cqZAp">
          <node concept="37vLTI" id="4Iz7iG3K2My" role="3clFbG">
            <node concept="2c44tf" id="4Iz7iG3K3lS" role="37vLTx">
              <node concept="3uibUv" id="4Iz7iG3K4O7" role="2c44tc">
                <ref role="3uigEE" to="e2lb:~String" resolve="String" />
              </node>
            </node>
            <node concept="2OqwBi" id="4Iz7iG3K1S2" role="37vLTJ">
              <node concept="37vLTw" id="4Iz7iG3K1MM" role="2Oq$k0">
                <ref role="3cqZAo" node="4Iz7iG3K0B7" resolve="stringNode" />
              </node>
              <node concept="3TrEf2" id="4Iz7iG3K2j2" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:4VkOLwjf83e" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4Iz7iG3Kkbz" role="3cqZAp">
          <node concept="37vLTI" id="4Iz7iG3Kl7P" role="3clFbG">
            <node concept="2ShNRf" id="4Iz7iG3KlaW" role="37vLTx">
              <node concept="3zrR0B" id="4Iz7iG3KlaU" role="2ShVmc">
                <node concept="3Tqbb2" id="4Iz7iG3KlaV" role="3zrR0E">
                  <ref role="ehGHo" to="80av:74LOPGM6KOt" resolve="instanceMethodDeclarationEx" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="4Iz7iG3Kkpg" role="37vLTJ">
              <node concept="13iPFW" id="4Iz7iG3Kkbx" role="2Oq$k0" />
              <node concept="3TrEf2" id="4Iz7iG3KkBL" role="2OqNvi">
                <ref role="3Tt5mk" to="80av:4Iz7iG3JZ1T" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4Iz7iG3P6My" role="3cqZAp">
          <node concept="37vLTI" id="4Iz7iG3Paen" role="3clFbG">
            <node concept="2ShNRf" id="4Iz7iG3Pa_V" role="37vLTx">
              <node concept="3zrR0B" id="4Iz7iG3Pa_u" role="2ShVmc">
                <node concept="3Tqbb2" id="4Iz7iG3Pa_v" role="3zrR0E">
                  <ref role="ehGHo" to="tpee:fzcqZ_H" resolve="VoidType" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="4Iz7iG3P7xJ" role="37vLTJ">
              <node concept="2OqwBi" id="4Iz7iG3P6Z5" role="2Oq$k0">
                <node concept="13iPFW" id="4Iz7iG3P6Mw" role="2Oq$k0" />
                <node concept="3TrEf2" id="4Iz7iG3P7dA" role="2OqNvi">
                  <ref role="3Tt5mk" to="80av:4Iz7iG3JZ1T" />
                </node>
              </node>
              <node concept="3TrEf2" id="4Iz7iG3P9aA" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:fzclF7X" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4Iz7iG3KllY" role="3cqZAp">
          <node concept="37vLTI" id="4Iz7iG3KoLG" role="3clFbG">
            <node concept="Xl_RD" id="4Iz7iG3KoRk" role="37vLTx">
              <property role="Xl_RC" value="arbitraged" />
            </node>
            <node concept="2OqwBi" id="4Iz7iG3Km3J" role="37vLTJ">
              <node concept="2OqwBi" id="4Iz7iG3Klxn" role="2Oq$k0">
                <node concept="13iPFW" id="4Iz7iG3KllW" role="2Oq$k0" />
                <node concept="3TrEf2" id="4Iz7iG3KlMC" role="2OqNvi">
                  <ref role="3Tt5mk" to="80av:4Iz7iG3JZ1T" />
                </node>
              </node>
              <node concept="3TrcHB" id="4Iz7iG3KnJY" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4Iz7iG3Kq$D" role="3cqZAp">
          <node concept="37vLTI" id="4Iz7iG3KsXO" role="3clFbG">
            <node concept="2ShNRf" id="4Iz7iG3Kt1r" role="37vLTx">
              <node concept="3zrR0B" id="4Iz7iG3Kt1p" role="2ShVmc">
                <node concept="3Tqbb2" id="4Iz7iG3Kt1q" role="3zrR0E">
                  <ref role="ehGHo" to="tpee:gFTm6Wc" resolve="PrivateVisibility" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="4Iz7iG3KriT" role="37vLTJ">
              <node concept="2OqwBi" id="4Iz7iG3KqKt" role="2Oq$k0">
                <node concept="13iPFW" id="4Iz7iG3Kq$B" role="2Oq$k0" />
                <node concept="3TrEf2" id="4Iz7iG3Kr1M" role="2OqNvi">
                  <ref role="3Tt5mk" to="80av:4Iz7iG3JZ1T" />
                </node>
              </node>
              <node concept="3TrEf2" id="4Iz7iG3Ks7G" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:h9B3oxE" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4Iz7iG3K4UW" role="3cqZAp">
          <node concept="2OqwBi" id="4Iz7iG3K8nj" role="3clFbG">
            <node concept="2OqwBi" id="4Iz7iG3K5sD" role="2Oq$k0">
              <node concept="2OqwBi" id="4Iz7iG3K4WT" role="2Oq$k0">
                <node concept="13iPFW" id="4Iz7iG3K4UU" role="2Oq$k0" />
                <node concept="3TrEf2" id="4Iz7iG3K5bq" role="2OqNvi">
                  <ref role="3Tt5mk" to="80av:4Iz7iG3JZ1T" />
                </node>
              </node>
              <node concept="3Tsc0h" id="4Iz7iG3K6hd" role="2OqNvi">
                <ref role="3TtcxE" to="tpee:fzclF7Y" />
              </node>
            </node>
            <node concept="TSZUe" id="4Iz7iG3Ke7E" role="2OqNvi">
              <node concept="37vLTw" id="4Iz7iG3KelE" role="25WWJ7">
                <ref role="3cqZAo" node="4Iz7iG3K0B7" resolve="stringNode" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4Iz7iG3UYXJ" role="3cqZAp">
          <node concept="37vLTI" id="4Iz7iG3V4Si" role="3clFbG">
            <node concept="2ShNRf" id="4Iz7iG3V4T8" role="37vLTx">
              <node concept="3zrR0B" id="4Iz7iG3V53M" role="2ShVmc">
                <node concept="3Tqbb2" id="4Iz7iG3V53O" role="3zrR0E">
                  <ref role="ehGHo" to="tpee:fzclF80" resolve="StatementList" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="4Iz7iG3UZT4" role="37vLTJ">
              <node concept="2OqwBi" id="4Iz7iG3UZjc" role="2Oq$k0">
                <node concept="13iPFW" id="4Iz7iG3UYXH" role="2Oq$k0" />
                <node concept="3TrEf2" id="4Iz7iG3UZBP" role="2OqNvi">
                  <ref role="3Tt5mk" to="80av:4Iz7iG3JZ1T" />
                </node>
              </node>
              <node concept="3TrEf2" id="4Iz7iG3V1xV" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:fzclF7Z" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="4Iz7iG3UndL" role="3cqZAp">
          <node concept="2OqwBi" id="4Iz7iG3Us$6" role="3clFbG">
            <node concept="2OqwBi" id="4Iz7iG3UreM" role="2Oq$k0">
              <node concept="2OqwBi" id="4Iz7iG3UnUI" role="2Oq$k0">
                <node concept="2OqwBi" id="4Iz7iG3Unr6" role="2Oq$k0">
                  <node concept="13iPFW" id="4Iz7iG3UndJ" role="2Oq$k0" />
                  <node concept="3TrEf2" id="4Iz7iG3UnDB" role="2OqNvi">
                    <ref role="3Tt5mk" to="80av:4Iz7iG3JZ1T" />
                  </node>
                </node>
                <node concept="3TrEf2" id="4Iz7iG3UoM8" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:fzclF7Z" />
                </node>
              </node>
              <node concept="3Tsc0h" id="4Iz7iG3Ur_N" role="2OqNvi">
                <ref role="3TtcxE" to="tpee:fzcqZ_x" />
              </node>
            </node>
            <node concept="TSZUe" id="4Iz7iG3Uun6" role="2OqNvi">
              <node concept="2c44tf" id="4Iz7iG3UCEg" role="25WWJ7">
                <node concept="3SKdUt" id="4Iz7iG3UCRX" role="2c44tc">
                  <node concept="3SKdUq" id="4Iz7iG3UD3G" role="3SKWNk">
                    <property role="3SKdUp" value="input is parsed component" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4Iz7iG3KeAM" role="13h7CS">
      <property role="TrG5h" value="getNameForMember" />
      <node concept="3Tm1VV" id="4Iz7iG3KeAN" role="1B3o_S" />
      <node concept="17QB3L" id="4Iz7iG3KeSn" role="3clF45" />
      <node concept="3clFbS" id="4Iz7iG3KeAP" role="3clF47">
        <node concept="3clFbF" id="4Iz7iG3KeSE" role="3cqZAp">
          <node concept="3cpWs3" id="4Iz7iG3Kf4n" role="3clFbG">
            <node concept="2OqwBi" id="4Iz7iG3KgcG" role="3uHU7w">
              <node concept="37vLTw" id="4Iz7iG3Kf4_" role="2Oq$k0">
                <ref role="3cqZAo" node="4Iz7iG3KeSr" resolve="memberName" />
              </node>
              <node concept="2qgKlT" id="4Iz7iG3KgOh" role="2OqNvi">
                <ref role="37wK5l" node="1ZzbrikgekP" resolve="convertToCamelCase" />
              </node>
            </node>
            <node concept="Xl_RD" id="4Iz7iG3KeSD" role="3uHU7B">
              <property role="Xl_RC" value="convertFor" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="4Iz7iG3KeSr" role="3clF46">
        <property role="TrG5h" value="memberName" />
        <node concept="3Tqbb2" id="4Iz7iG3Kg00" role="1tU5fm">
          <ref role="ehGHo" to="80av:66tNEy7MCIO" resolve="Member" />
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4Iz7iG3Kipg" role="13h7CS">
      <property role="TrG5h" value="setReturnType" />
      <node concept="3Tm1VV" id="4Iz7iG3Kiph" role="1B3o_S" />
      <node concept="3cqZAl" id="4Iz7iG3Ki_G" role="3clF45" />
      <node concept="3clFbS" id="4Iz7iG3Kipj" role="3clF47">
        <node concept="34ab3g" id="4Iz7iG3PBFE" role="3cqZAp">
          <property role="35gtTG" value="info" />
          <node concept="Xl_RD" id="4Iz7iG3PBFG" role="34bqiv">
            <property role="Xl_RC" value="resetting returnType" />
          </node>
        </node>
        <node concept="3clFbF" id="4Iz7iG3KiA1" role="3cqZAp">
          <node concept="37vLTI" id="4Iz7iG3Kqfj" role="3clFbG">
            <node concept="37vLTw" id="4Iz7iG3Kqj4" role="37vLTx">
              <ref role="3cqZAo" node="4Iz7iG3Ki_K" resolve="retType" />
            </node>
            <node concept="2OqwBi" id="4Iz7iG3Kj9w" role="37vLTJ">
              <node concept="2OqwBi" id="4Iz7iG3KiB8" role="2Oq$k0">
                <node concept="13iPFW" id="4Iz7iG3KiA0" role="2Oq$k0" />
                <node concept="3TrEf2" id="4Iz7iG3KiPD" role="2OqNvi">
                  <ref role="3Tt5mk" to="80av:4Iz7iG3JZ1T" />
                </node>
              </node>
              <node concept="3TrEf2" id="4Iz7iG3KjYj" role="2OqNvi">
                <ref role="3Tt5mk" to="tpee:fzclF7X" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="4Iz7iG3Ki_K" role="3clF46">
        <property role="TrG5h" value="retType" />
        <node concept="3Tqbb2" id="4Iz7iG3Ki_J" role="1tU5fm">
          <ref role="ehGHo" to="tpee:g7uibYu" resolve="ClassifierType" />
        </node>
      </node>
    </node>
  </node>
</model>

