<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:98601d8b-ce41-42bd-bd89-e3c1f18f4aa7(LogScrapper.intentions)">
  <persistence version="9" />
  <languages>
    <use id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions" version="-1" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="k7g3" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.util(JDK/java.util@java_stub)" />
    <import index="ec5l" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/f:java_stub#8865b7a8-5271-43d3-884c-6fd1d9cfdd34#org.jetbrains.mps.openapi.model(MPS.OpenAPI/org.jetbrains.mps.openapi.model@java_stub)" />
    <import index="srng" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/f:java_stub#1ed103c3-3aa6-49b7-9c21-6765ee11f224#jetbrains.mps.openapi.editor(MPS.Editor/jetbrains.mps.openapi.editor@java_stub)" />
    <import index="y596" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/f:java_stub#1ed103c3-3aa6-49b7-9c21-6765ee11f224#jetbrains.mps.openapi.editor.selection(MPS.Editor/jetbrains.mps.openapi.editor.selection@java_stub)" />
    <import index="nu8v" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/f:java_stub#1ed103c3-3aa6-49b7-9c21-6765ee11f224#jetbrains.mps.openapi.editor.cells(MPS.Editor/jetbrains.mps.openapi.editor.cells@java_stub)" />
    <import index="e2lb" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.lang(JDK/java.lang@java_stub)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="80av" ref="r:304512bc-2ab8-4538-a01c-29a28f4fb5a1(LogScrapper.structure)" />
  </imports>
  <registry>
    <language id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts">
      <concept id="1194033889146" name="jetbrains.mps.lang.sharedConcepts.structure.ConceptFunctionParameter_editorContext" flags="nn" index="1XNTG" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1224071154655" name="jetbrains.mps.baseLanguage.structure.AsExpression" flags="nn" index="0kSF2">
        <child id="1224071154657" name="classifierType" index="0kSFW" />
        <child id="1224071154656" name="expression" index="0kSFX" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1215695189714" name="jetbrains.mps.baseLanguage.structure.PlusAssignmentExpression" flags="nn" index="d57v9" />
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1153952380246" name="jetbrains.mps.baseLanguage.structure.TryStatement" flags="nn" index="2GUZhq">
        <child id="1153952416686" name="body" index="2GV8ay" />
        <child id="1153952429843" name="finallyBody" index="2GVbov" />
        <child id="1164903700860" name="catchClause" index="TEXxN" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1164903280175" name="jetbrains.mps.baseLanguage.structure.CatchClause" flags="nn" index="TDmWw">
        <child id="1164903359218" name="catchBody" index="TDEfX" />
        <child id="1164903359217" name="throwable" index="TDEfY" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions">
      <concept id="1192794744107" name="jetbrains.mps.lang.intentions.structure.IntentionDeclaration" flags="ig" index="2S6QgY" />
      <concept id="1192794782375" name="jetbrains.mps.lang.intentions.structure.DescriptionBlock" flags="in" index="2S6ZIM" />
      <concept id="1192795771125" name="jetbrains.mps.lang.intentions.structure.IsApplicableBlock" flags="in" index="2SaL7w" />
      <concept id="1192795911897" name="jetbrains.mps.lang.intentions.structure.ExecuteBlock" flags="in" index="2Sbjvc" />
      <concept id="1192796902958" name="jetbrains.mps.lang.intentions.structure.ConceptFunctionParameter_node" flags="nn" index="2Sf5sV" />
      <concept id="2522969319638091381" name="jetbrains.mps.lang.intentions.structure.BaseIntentionDeclaration" flags="ig" index="2ZfUlf">
        <property id="2522969319638091386" name="isAvailableInChildNodes" index="2ZfUl0" />
        <reference id="2522969319638198290" name="forConcept" index="2ZfgGC" />
        <child id="2522969319638198291" name="executeFunction" index="2ZfgGD" />
        <child id="2522969319638093995" name="isApplicableFunction" index="2ZfVeh" />
        <child id="2522969319638093993" name="descriptionFunction" index="2ZfVej" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="1167227138527" name="jetbrains.mps.baseLanguage.logging.structure.LogStatement" flags="nn" index="34ab3g">
        <property id="1167228628751" name="hasException" index="34fQS0" />
        <property id="1167245565795" name="severity" index="35gtTG" />
        <child id="1167227463056" name="logExpression" index="34bqiv" />
        <child id="1167227561449" name="exception" index="34bMjA" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="2644386474300074836" name="jetbrains.mps.lang.smodel.structure.ConceptIdRefExpression" flags="nn" index="35c_gC">
        <reference id="2644386474300074837" name="conceptDeclaration" index="35c_gD" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <reference id="1140138128738" name="concept" index="1PxNhF" />
        <child id="1140138123956" name="leftExpression" index="1PxMeX" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1151689724996" name="jetbrains.mps.baseLanguage.collections.structure.SequenceType" flags="in" index="A3Dl8">
        <child id="1151689745422" name="elementType" index="A3Ik2" />
      </concept>
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="2S6QgY" id="1ZzbrikjN5R">
    <property role="TrG5h" value="PatIntent" />
    <property role="2ZfUl0" value="true" />
    <ref role="2ZfgGC" to="80av:1ZzbrikhxxY" resolve="Pattern" />
    <node concept="2Sbjvc" id="1ZzbrikjN5S" role="2ZfgGD">
      <node concept="3clFbS" id="1ZzbrikjN5T" role="2VODD2">
        <node concept="2GUZhq" id="1ZzbrikjT6D" role="3cqZAp">
          <node concept="TDmWw" id="1ZzbrikjTgx" role="TEXxN">
            <node concept="3clFbS" id="1ZzbrikjTgy" role="TDEfX">
              <node concept="34ab3g" id="1ZzbrikjTl_" role="3cqZAp">
                <property role="35gtTG" value="error" />
                <property role="34fQS0" value="true" />
                <node concept="Xl_RD" id="1ZzbrikjTlB" role="34bqiv">
                  <property role="Xl_RC" value="Exception was caught in intention" />
                </node>
                <node concept="37vLTw" id="1ZzbrikjTlD" role="34bMjA">
                  <ref role="3cqZAo" node="1ZzbrikjTgz" resolve="ex" />
                </node>
              </node>
            </node>
            <node concept="3cpWsn" id="1ZzbrikjTgz" role="TDEfY">
              <property role="TrG5h" value="ex" />
              <node concept="3uibUv" id="1ZzbrikjTkv" role="1tU5fm">
                <ref role="3uigEE" to="e2lb:~Exception" resolve="Exception" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="1ZzbrikjT6F" role="2GV8ay">
            <node concept="3cpWs8" id="1ZzbrikjQF5" role="3cqZAp">
              <node concept="3cpWsn" id="1ZzbrikjQF6" role="3cpWs9">
                <property role="TrG5h" value="selection" />
                <node concept="3uibUv" id="1ZzbrikjQF4" role="1tU5fm">
                  <ref role="3uigEE" to="y596:~Selection" resolve="Selection" />
                </node>
                <node concept="2OqwBi" id="1ZzbrikjQF7" role="33vP2m">
                  <node concept="2OqwBi" id="1ZzbrikjQF8" role="2Oq$k0">
                    <node concept="1XNTG" id="1ZzbrikjQF9" role="2Oq$k0" />
                    <node concept="liA8E" id="1ZzbrikjQFa" role="2OqNvi">
                      <ref role="37wK5l" to="srng:~EditorContext.getSelectionManager():jetbrains.mps.openapi.editor.selection.SelectionManager" resolve="getSelectionManager" />
                    </node>
                  </node>
                  <node concept="liA8E" id="1ZzbrikjQFb" role="2OqNvi">
                    <ref role="37wK5l" to="y596:~SelectionManager.getSelection():jetbrains.mps.openapi.editor.selection.Selection" resolve="getSelection" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="1Zzbrikn9uC" role="3cqZAp">
              <node concept="3cpWsn" id="1Zzbrikn9uD" role="3cpWs9">
                <property role="TrG5h" value="selectedCells" />
                <node concept="3uibUv" id="1Zzbrikn9ur" role="1tU5fm">
                  <ref role="3uigEE" to="k7g3:~List" resolve="List" />
                  <node concept="3uibUv" id="1Zzbrikn9uu" role="11_B2D">
                    <ref role="3uigEE" to="nu8v:~EditorCell" resolve="EditorCell" />
                  </node>
                </node>
                <node concept="2OqwBi" id="1Zzbrikn9uE" role="33vP2m">
                  <node concept="37vLTw" id="1Zzbrikn9uF" role="2Oq$k0">
                    <ref role="3cqZAo" node="1ZzbrikjQF6" resolve="selection" />
                  </node>
                  <node concept="liA8E" id="1Zzbrikn9uG" role="2OqNvi">
                    <ref role="37wK5l" to="y596:~Selection.getSelectedCells():java.util.List" resolve="getSelectedCells" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="1ZzbrikqxT3" role="3cqZAp">
              <node concept="3cpWsn" id="1ZzbrikqxT6" role="3cpWs9">
                <property role="TrG5h" value="start" />
                <node concept="10Oyi0" id="1ZzbrikqxT1" role="1tU5fm" />
                <node concept="3cmrfG" id="1ZzbrikqCSg" role="33vP2m">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="1ZzbrikqyBi" role="3cqZAp">
              <node concept="3cpWsn" id="1ZzbrikqyBl" role="3cpWs9">
                <property role="TrG5h" value="end" />
                <node concept="10Oyi0" id="1ZzbrikqyBg" role="1tU5fm" />
                <node concept="3cmrfG" id="1ZzbrikqD6E" role="33vP2m">
                  <property role="3cmrfH" value="0" />
                </node>
              </node>
            </node>
            <node concept="3cpWs8" id="1ZzbrikpDnX" role="3cqZAp">
              <node concept="3cpWsn" id="1ZzbrikpDo0" role="3cpWs9">
                <property role="TrG5h" value="text" />
                <node concept="17QB3L" id="1ZzbrikpDnV" role="1tU5fm" />
                <node concept="Xl_RD" id="1ZzbrikpDvy" role="33vP2m">
                  <property role="Xl_RC" value="" />
                </node>
              </node>
            </node>
            <node concept="2Gpval" id="1Zzbrikn9Ec" role="3cqZAp">
              <node concept="2GrKxI" id="1Zzbrikn9Ef" role="2Gsz3X">
                <property role="TrG5h" value="cll" />
              </node>
              <node concept="3clFbS" id="1Zzbrikn9Ei" role="2LFqv$">
                <node concept="3cpWs8" id="1Zzbrikq5KA" role="3cqZAp">
                  <node concept="3cpWsn" id="1Zzbrikq5KB" role="3cpWs9">
                    <property role="TrG5h" value="label" />
                    <node concept="3uibUv" id="1Zzbrikq5Kl" role="1tU5fm">
                      <ref role="3uigEE" to="nu8v:~EditorCell_Label" resolve="EditorCell_Label" />
                    </node>
                    <node concept="0kSF2" id="1Zzbrikq5KC" role="33vP2m">
                      <node concept="3uibUv" id="1Zzbrikq5KD" role="0kSFW">
                        <ref role="3uigEE" to="nu8v:~EditorCell_Label" resolve="EditorCell_Label" />
                      </node>
                      <node concept="2GrUjf" id="1Zzbrikq5KE" role="0kSFX">
                        <ref role="2Gs0qQ" node="1Zzbrikn9Ef" resolve="cll" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="1ZzbrikpEW5" role="3cqZAp">
                  <node concept="d57v9" id="1ZzbrikpF1L" role="3clFbG">
                    <node concept="2OqwBi" id="1ZzbrikpFL1" role="37vLTx">
                      <node concept="37vLTw" id="1Zzbrikq5KF" role="2Oq$k0">
                        <ref role="3cqZAo" node="1Zzbrikq5KB" resolve="label" />
                      </node>
                      <node concept="liA8E" id="1ZzbrikpFY$" role="2OqNvi">
                        <ref role="37wK5l" to="nu8v:~EditorCell_Label.getSelectedText():java.lang.String" resolve="getSelectedText" />
                      </node>
                    </node>
                    <node concept="37vLTw" id="1ZzbrikpEW4" role="37vLTJ">
                      <ref role="3cqZAo" node="1ZzbrikpDo0" resolve="text" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="1Zzbrikqx6g" role="3cqZAp">
                  <node concept="37vLTI" id="1Zzbrikqzd_" role="3clFbG">
                    <node concept="37vLTw" id="1ZzbrikqzhW" role="37vLTJ">
                      <ref role="3cqZAo" node="1ZzbrikqxT6" resolve="start" />
                    </node>
                    <node concept="2OqwBi" id="1Zzbrikqx9T" role="37vLTx">
                      <node concept="37vLTw" id="1Zzbrikqx6e" role="2Oq$k0">
                        <ref role="3cqZAo" node="1Zzbrikq5KB" resolve="label" />
                      </node>
                      <node concept="liA8E" id="1Zzbrikqxor" role="2OqNvi">
                        <ref role="37wK5l" to="nu8v:~EditorCell_Label.getSelectionStart():int" resolve="getSelectionStart" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="1Zzbrikqzr4" role="3cqZAp">
                  <node concept="37vLTI" id="1ZzbrikqzZl" role="3clFbG">
                    <node concept="2OqwBi" id="1Zzbrikq$89" role="37vLTx">
                      <node concept="37vLTw" id="1Zzbrikq$7j" role="2Oq$k0">
                        <ref role="3cqZAo" node="1Zzbrikq5KB" resolve="label" />
                      </node>
                      <node concept="liA8E" id="1Zzbrikq$mO" role="2OqNvi">
                        <ref role="37wK5l" to="nu8v:~EditorCell_Label.getSelectionEnd():int" resolve="getSelectionEnd" />
                      </node>
                    </node>
                    <node concept="37vLTw" id="1Zzbrikqzr2" role="37vLTJ">
                      <ref role="3cqZAo" node="1ZzbrikqyBl" resolve="end" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="37vLTw" id="1Zzbrikn9LC" role="2GsD0m">
                <ref role="3cqZAo" node="1Zzbrikn9uD" resolve="selectedCells" />
              </node>
            </node>
            <node concept="3clFbJ" id="1ZzbrikpGcV" role="3cqZAp">
              <node concept="3clFbS" id="1ZzbrikpGcX" role="3clFbx">
                <node concept="3cpWs8" id="74LOPGMcbsu" role="3cqZAp">
                  <node concept="3cpWsn" id="74LOPGMcbsv" role="3cpWs9">
                    <property role="TrG5h" value="seq" />
                    <node concept="A3Dl8" id="74LOPGMcbso" role="1tU5fm">
                      <node concept="3Tqbb2" id="74LOPGMcbsr" role="A3Ik2">
                        <ref role="ehGHo" to="80av:1ZzbrikhPdP" resolve="GroupItem" />
                      </node>
                    </node>
                    <node concept="2OqwBi" id="74LOPGMcbsw" role="33vP2m">
                      <node concept="2OqwBi" id="74LOPGMcbsx" role="2Oq$k0">
                        <node concept="2Sf5sV" id="74LOPGMcbsy" role="2Oq$k0" />
                        <node concept="3Tsc0h" id="74LOPGMcbsz" role="2OqNvi">
                          <ref role="3TtcxE" to="80av:1Zzbrikhxyq" />
                        </node>
                      </node>
                      <node concept="3zZkjj" id="74LOPGMcbs$" role="2OqNvi">
                        <node concept="1bVj0M" id="74LOPGMcbs_" role="23t8la">
                          <node concept="3clFbS" id="74LOPGMcbsA" role="1bW5cS">
                            <node concept="3clFbF" id="74LOPGMcbsB" role="3cqZAp">
                              <node concept="1Wc70l" id="74LOPGMcbsC" role="3clFbG">
                                <node concept="3eOSWO" id="74LOPGMcbsD" role="3uHU7w">
                                  <node concept="3cmrfG" id="74LOPGMcbsE" role="3uHU7w">
                                    <property role="3cmrfH" value="0" />
                                  </node>
                                  <node concept="2OqwBi" id="74LOPGMcbsF" role="3uHU7B">
                                    <node concept="1PxgMI" id="74LOPGMcbsG" role="2Oq$k0">
                                      <ref role="1PxNhF" to="80av:66tNEy7MCIO" resolve="Member" />
                                      <node concept="37vLTw" id="74LOPGMcbsH" role="1PxMeX">
                                        <ref role="3cqZAo" node="74LOPGMcbsN" resolve="it" />
                                      </node>
                                    </node>
                                    <node concept="3TrcHB" id="74LOPGMcC1E" role="2OqNvi">
                                      <ref role="3TsBF5" to="80av:1Zzbrikhnbr" resolve="index" />
                                    </node>
                                  </node>
                                </node>
                                <node concept="2OqwBi" id="74LOPGMcbsJ" role="3uHU7B">
                                  <node concept="37vLTw" id="74LOPGMcbsK" role="2Oq$k0">
                                    <ref role="3cqZAo" node="74LOPGMcbsN" resolve="it" />
                                  </node>
                                  <node concept="1mIQ4w" id="74LOPGMcbsL" role="2OqNvi">
                                    <node concept="chp4Y" id="74LOPGMcbsM" role="cj9EA">
                                      <ref role="cht4Q" to="80av:66tNEy7MCIO" resolve="Member" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="Rh6nW" id="74LOPGMcbsN" role="1bW2Oz">
                            <property role="TrG5h" value="it" />
                            <node concept="2jxLKc" id="74LOPGMcbsO" role="1tU5fm" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="1ZzbrikpQN9" role="3cqZAp">
                  <node concept="3cpWsn" id="1ZzbrikpQNc" role="3cpWs9">
                    <property role="TrG5h" value="index" />
                    <node concept="10Oyi0" id="1ZzbrikpQN7" role="1tU5fm" />
                    <node concept="3K4zz7" id="74LOPGMccMK" role="33vP2m">
                      <node concept="3cmrfG" id="74LOPGMcfyS" role="3K4GZi">
                        <property role="3cmrfH" value="0" />
                      </node>
                      <node concept="3y3z36" id="74LOPGMcdyV" role="3K4Cdx">
                        <node concept="10Nm6u" id="74LOPGMcdMq" role="3uHU7w" />
                        <node concept="37vLTw" id="74LOPGMcd27" role="3uHU7B">
                          <ref role="3cqZAo" node="74LOPGMcbsv" resolve="seq" />
                        </node>
                      </node>
                      <node concept="2OqwBi" id="74LOPGMce7K" role="3K4E3e">
                        <node concept="37vLTw" id="74LOPGMcdQZ" role="2Oq$k0">
                          <ref role="3cqZAo" node="74LOPGMcbsv" resolve="seq" />
                        </node>
                        <node concept="34oBXx" id="74LOPGMceWR" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="1ZzbrikpHEA" role="3cqZAp">
                  <node concept="3cpWsn" id="1ZzbrikpHED" role="3cpWs9">
                    <property role="TrG5h" value="member" />
                    <node concept="3Tqbb2" id="1ZzbrikpHE$" role="1tU5fm">
                      <ref role="ehGHo" to="80av:66tNEy7MCIO" resolve="Member" />
                    </node>
                    <node concept="2ShNRf" id="1ZzbrikpHJr" role="33vP2m">
                      <node concept="3zrR0B" id="1ZzbrikpHQh" role="2ShVmc">
                        <node concept="3Tqbb2" id="1ZzbrikpHQj" role="3zrR0E">
                          <ref role="ehGHo" to="80av:66tNEy7MCIO" resolve="Member" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="1ZzbrikpHVw" role="3cqZAp">
                  <node concept="37vLTI" id="1ZzbrikpJ3t" role="3clFbG">
                    <node concept="37vLTw" id="1ZzbrikpJ8w" role="37vLTx">
                      <ref role="3cqZAo" node="1ZzbrikpDo0" resolve="text" />
                    </node>
                    <node concept="2OqwBi" id="1ZzbrikpHXC" role="37vLTJ">
                      <node concept="37vLTw" id="1ZzbrikpHVu" role="2Oq$k0">
                        <ref role="3cqZAo" node="1ZzbrikpHED" resolve="member" />
                      </node>
                      <node concept="3TrcHB" id="1ZzbrikpIFY" role="2OqNvi">
                        <ref role="3TsBF5" to="80av:1ZzbrikhPg2" resolve="pattern" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="1ZzbrikpVeV" role="3cqZAp">
                  <node concept="37vLTI" id="1ZzbrikpWgD" role="3clFbG">
                    <node concept="3cpWs3" id="1ZzbrikpWBU" role="37vLTx">
                      <node concept="3cmrfG" id="1ZzbrikpWC0" role="3uHU7w">
                        <property role="3cmrfH" value="1" />
                      </node>
                      <node concept="37vLTw" id="1ZzbrikpWoz" role="3uHU7B">
                        <ref role="3cqZAo" node="1ZzbrikpQNc" resolve="index" />
                      </node>
                    </node>
                    <node concept="2OqwBi" id="1ZzbrikpVos" role="37vLTJ">
                      <node concept="37vLTw" id="1ZzbrikpVeT" role="2Oq$k0">
                        <ref role="3cqZAo" node="1ZzbrikpHED" resolve="member" />
                      </node>
                      <node concept="3TrcHB" id="1ZzbrikpV_I" role="2OqNvi">
                        <ref role="3TsBF5" to="80av:1Zzbrikhnbr" resolve="index" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="1ZzbrikpJen" role="3cqZAp">
                  <node concept="2OqwBi" id="1ZzbrikpKjo" role="3clFbG">
                    <node concept="2OqwBi" id="1ZzbrikpJg0" role="2Oq$k0">
                      <node concept="2Sf5sV" id="1ZzbrikpJel" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="1ZzbrikpJOr" role="2OqNvi">
                        <ref role="3TtcxE" to="80av:1Zzbrikhxyq" />
                      </node>
                    </node>
                    <node concept="TSZUe" id="1ZzbrikpMaw" role="2OqNvi">
                      <node concept="37vLTw" id="1ZzbrikpMf7" role="25WWJ7">
                        <ref role="3cqZAo" node="1ZzbrikpHED" resolve="member" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs8" id="1Zzbrikquo4" role="3cqZAp">
                  <node concept="3cpWsn" id="1Zzbrikquo5" role="3cpWs9">
                    <property role="TrG5h" value="nodez" />
                    <node concept="3Tqbb2" id="1Zzbrikquo1" role="1tU5fm">
                      <ref role="ehGHo" to="tpee:f$Xl_Og" resolve="StringLiteral" />
                    </node>
                    <node concept="2OqwBi" id="1ZzbrikqDZ2" role="33vP2m">
                      <node concept="2Sf5sV" id="1ZzbrikqDVJ" role="2Oq$k0" />
                      <node concept="3TrEf2" id="1ZzbrikqEcB" role="2OqNvi">
                        <ref role="3Tt5mk" to="80av:1ZzbriknkcM" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="1Zzbrikqveb" role="3cqZAp">
                  <node concept="3clFbS" id="1Zzbrikqved" role="3clFbx">
                    <node concept="3cpWs8" id="1Zzbrikq$JX" role="3cqZAp">
                      <node concept="3cpWsn" id="1Zzbrikq$JY" role="3cpWs9">
                        <property role="TrG5h" value="value" />
                        <node concept="17QB3L" id="1Zzbrikq$JV" role="1tU5fm" />
                        <node concept="2OqwBi" id="1Zzbrikq$JZ" role="33vP2m">
                          <node concept="37vLTw" id="1Zzbrikq$K0" role="2Oq$k0">
                            <ref role="3cqZAo" node="1Zzbrikquo5" resolve="nodez" />
                          </node>
                          <node concept="3TrcHB" id="1Zzbrikq$K1" role="2OqNvi">
                            <ref role="3TsBF5" to="tpee:f$Xl_Oh" resolve="value" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3cpWs8" id="1ZzbrikqB2q" role="3cqZAp">
                      <node concept="3cpWsn" id="1ZzbrikqB2r" role="3cpWs9">
                        <property role="TrG5h" value="toString" />
                        <node concept="3uibUv" id="1ZzbrikqB2h" role="1tU5fm">
                          <ref role="3uigEE" to="e2lb:~String" resolve="String" />
                        </node>
                        <node concept="2OqwBi" id="1ZzbrikqB2s" role="33vP2m">
                          <node concept="2OqwBi" id="1ZzbrikqB2t" role="2Oq$k0">
                            <node concept="2ShNRf" id="1ZzbrikqB2u" role="2Oq$k0">
                              <node concept="1pGfFk" id="1ZzbrikqB2v" role="2ShVmc">
                                <ref role="37wK5l" to="e2lb:~StringBuilder.&lt;init&gt;(java.lang.String)" resolve="StringBuilder" />
                                <node concept="37vLTw" id="1ZzbrikqB2w" role="37wK5m">
                                  <ref role="3cqZAo" node="1Zzbrikq$JY" resolve="value" />
                                </node>
                              </node>
                            </node>
                            <node concept="liA8E" id="1ZzbrikqB2x" role="2OqNvi">
                              <ref role="37wK5l" to="e2lb:~StringBuilder.delete(int,int):java.lang.StringBuilder" resolve="delete" />
                              <node concept="37vLTw" id="1ZzbrikqB2y" role="37wK5m">
                                <ref role="3cqZAo" node="1ZzbrikqxT6" resolve="start" />
                              </node>
                              <node concept="37vLTw" id="1ZzbrikqB2z" role="37wK5m">
                                <ref role="3cqZAo" node="1ZzbrikqyBl" resolve="end" />
                              </node>
                            </node>
                          </node>
                          <node concept="liA8E" id="1ZzbrikqB2$" role="2OqNvi">
                            <ref role="37wK5l" to="e2lb:~StringBuilder.toString():java.lang.String" resolve="toString" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3clFbF" id="1Zzbrikq$sv" role="3cqZAp">
                      <node concept="37vLTI" id="1ZzbrikqBUY" role="3clFbG">
                        <node concept="37vLTw" id="1ZzbrikqC1L" role="37vLTx">
                          <ref role="3cqZAo" node="1ZzbrikqB2r" resolve="toString" />
                        </node>
                        <node concept="2OqwBi" id="1ZzbrikqBhf" role="37vLTJ">
                          <node concept="37vLTw" id="1ZzbrikqBfo" role="2Oq$k0">
                            <ref role="3cqZAo" node="1Zzbrikquo5" resolve="nodez" />
                          </node>
                          <node concept="3TrcHB" id="1ZzbrikqBvi" role="2OqNvi">
                            <ref role="3TsBF5" to="tpee:f$Xl_Oh" resolve="value" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3y3z36" id="1Zzbrikqv$5" role="3clFbw">
                    <node concept="10Nm6u" id="1Zzbrikqv$p" role="3uHU7w" />
                    <node concept="37vLTw" id="1Zzbrikqvvl" role="3uHU7B">
                      <ref role="3cqZAo" node="1Zzbrikquo5" resolve="nodez" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3eOSWO" id="1ZzbrikpHyA" role="3clFbw">
                <node concept="3cmrfG" id="1ZzbrikpHyG" role="3uHU7w">
                  <property role="3cmrfH" value="0" />
                </node>
                <node concept="2OqwBi" id="1ZzbrikpGuG" role="3uHU7B">
                  <node concept="37vLTw" id="1ZzbrikpGli" role="2Oq$k0">
                    <ref role="3cqZAo" node="1ZzbrikpDo0" resolve="text" />
                  </node>
                  <node concept="liA8E" id="1ZzbrikpHcc" role="2OqNvi">
                    <ref role="37wK5l" to="e2lb:~String.length():int" resolve="length" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="1ZzbrikpG4V" role="3cqZAp" />
          </node>
          <node concept="3clFbS" id="1ZzbrikjT6G" role="2GVbov" />
        </node>
      </node>
    </node>
    <node concept="2S6ZIM" id="1ZzbrikjN5U" role="2ZfVej">
      <node concept="3clFbS" id="1ZzbrikjN5V" role="2VODD2">
        <node concept="3clFbF" id="1ZzbrikjNl$" role="3cqZAp">
          <node concept="Xl_RD" id="1ZzbrikjNlz" role="3clFbG">
            <property role="Xl_RC" value="Convert to Pattern" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2SaL7w" id="1ZzbrikqSlk" role="2ZfVeh">
      <node concept="3clFbS" id="1ZzbrikqSll" role="2VODD2">
        <node concept="3clFbF" id="1ZzbrikqSJm" role="3cqZAp">
          <node concept="3clFbC" id="1ZzbrikqTwY" role="3clFbG">
            <node concept="35c_gC" id="1ZzbrikqTCq" role="3uHU7w">
              <ref role="35c_gD" to="tpee:f$Xl_Og" resolve="StringLiteral" />
            </node>
            <node concept="2OqwBi" id="1ZzbrikqT6x" role="3uHU7B">
              <node concept="2OqwBi" id="1ZzbrikqSMu" role="2Oq$k0">
                <node concept="1XNTG" id="1ZzbrikqSJl" role="2Oq$k0" />
                <node concept="liA8E" id="1ZzbrikqSWF" role="2OqNvi">
                  <ref role="37wK5l" to="srng:~EditorContext.getSelectedNode():org.jetbrains.mps.openapi.model.SNode" resolve="getSelectedNode" />
                </node>
              </node>
              <node concept="liA8E" id="1ZzbrikqTmC" role="2OqNvi">
                <ref role="37wK5l" to="ec5l:~SNode.getConcept():org.jetbrains.mps.openapi.language.SConcept" resolve="getConcept" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

