<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:304512bc-2ab8-4538-a01c-29a28f4fb5a1(LogScrapper.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="4fqr" ref="r:fa713d69-08ea-4732-b1f2-cb07f9e103ef(jetbrains.mps.execution.util.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="4628067390765907488" name="conceptShortDescription" index="R4oN_" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="1oo09loik4O">
    <property role="TrG5h" value="LogFilSelector" />
    <property role="34LRSv" value="loadLog" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="31zJxSiUUUU" role="1TKVEl">
      <property role="TrG5h" value="fileLocation" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7HoD$xn5r91" role="1TKVEl">
      <property role="TrG5h" value="fileNme" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="31zJxSiULHM" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="loggedItems" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="31zJxSiUO6T" resolve="LoggedItemReference" />
    </node>
    <node concept="1TJgyj" id="_IX99AD57A" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="exporter" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="7HoD$xmJ9nh" resolve="Exporter" />
    </node>
    <node concept="1TJgyj" id="7HoD$xmLSCU" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="groupItem" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="1ZzbrikhPdP" resolve="GroupItem" />
    </node>
    <node concept="PrWs8" id="1oo09loiqdP" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="1oo09lok4pT" role="PzmwI">
      <ref role="PrY4T" to="4fqr:431DWIovi3l" resolve="IMainClass" />
    </node>
  </node>
  <node concept="1TIwiD" id="66tNEy7Mgmx">
    <property role="TrG5h" value="LoggedItem" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="item" />
    <ref role="1TJDcQ" to="tpee:fz12cDA" resolve="ClassConcept" />
    <node concept="1TJgyj" id="1Zzbrikhzf4" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pattern" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1ZzbrikhxxY" resolve="Pattern" />
    </node>
    <node concept="1TJgyj" id="1By676OcpLm" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="transformers" />
      <ref role="20lvS9" node="74LOPGM6KOt" resolve="instanceMethodDeclarationEx" />
    </node>
    <node concept="PrWs8" id="66tNEy7Mgn0" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="66tNEy7MCIO">
    <property role="TrG5h" value="Member" />
    <property role="34LRSv" value="capture expression" />
    <property role="R4oN_" value="Each Capture expression captures text item from scrapped file" />
    <ref role="1TJDcQ" to="tpee:fz12cDC" resolve="FieldDeclaration" />
    <node concept="1TJgyj" id="4Iz7iG3GjXD" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="converter" />
      <ref role="20lvS9" node="4Iz7iG3JYy5" resolve="instanceMethodDeclarationExReference" />
    </node>
    <node concept="1TJgyi" id="1Zzbrikhnbr" role="1TKVEl">
      <property role="TrG5h" value="index" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="1ZzbrikhPg2" role="1TKVEl">
      <property role="TrG5h" value="pattern" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="1ZzbrikisZ0" role="1TKVEl">
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7HoD$xnidOJ" role="1TKVEl">
      <property role="TrG5h" value="regexp" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="PrWs8" id="1ZzbrikhPeL" role="PzmwI">
      <ref role="PrY4T" node="1ZzbrikhPdP" resolve="GroupItem" />
    </node>
  </node>
  <node concept="1TIwiD" id="1ZzbrikhxxY">
    <property role="TrG5h" value="Pattern" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="1ZzbriknkcM" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="line" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" to="tpee:f$Xl_Og" resolve="StringLiteral" />
    </node>
    <node concept="1TJgyj" id="1Zzbrikhxyq" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="expressions" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="1ZzbrikhPdP" resolve="GroupItem" />
    </node>
    <node concept="1TJgyj" id="_IX99AAH9S" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="identifier" />
      <ref role="20lvS9" node="_IX99A_ZKL" resolve="ItemIdentifier" />
    </node>
  </node>
  <node concept="PlHQZ" id="1ZzbrikhPdP">
    <property role="TrG5h" value="GroupItem" />
    <node concept="PrWs8" id="_IX99A_b0h" role="PrDN$">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="31zJxSiUO6T">
    <property role="TrG5h" value="LoggedItemReference" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="31zJxSiUO7l" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="logItem" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="66tNEy7Mgmx" resolve="LoggedItem" />
    </node>
  </node>
  <node concept="1TIwiD" id="_IX99A_ZKL">
    <property role="TrG5h" value="ItemIdentifier" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="_IX99AAwGa" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="item" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1ZzbrikhPdP" resolve="GroupItem" />
    </node>
  </node>
  <node concept="1TIwiD" id="_IX99AD3Ey">
    <property role="TrG5h" value="CSVExport" />
    <property role="34LRSv" value="csv export" />
    <ref role="1TJDcQ" node="7HoD$xmJ9nh" resolve="Exporter" />
    <node concept="1TJgyi" id="_IX99AEBxF" role="1TKVEl">
      <property role="TrG5h" value="fileName" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="_IX99AJOso" role="1TKVEl">
      <property role="TrG5h" value="separator" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7HoD$xn1kMN" role="1TKVEl">
      <property role="TrG5h" value="exportAll" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="7HoD$xn4yOc" role="1TKVEl">
      <property role="TrG5h" value="fileNme" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="7HoD$xmJ9nh">
    <property role="TrG5h" value="Exporter" />
    <property role="34LRSv" value="exporter" />
    <property role="R5$K7" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="_IX99AD3F_" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="logItem" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="66tNEy7Mgmx" resolve="LoggedItem" />
    </node>
    <node concept="1TJgyj" id="7HoD$xmMesq" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="groupItems" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="7HoD$xmMern" resolve="GroupItemReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="7HoD$xmMern">
    <property role="TrG5h" value="GroupItemReference" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7HoD$xmMerN" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="groupItem" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="1ZzbrikhPdP" resolve="GroupItem" />
    </node>
  </node>
  <node concept="1TIwiD" id="7HoD$xn6j2k">
    <property role="TrG5h" value="ExcelExporter" />
    <property role="34LRSv" value="excel export" />
    <ref role="1TJDcQ" node="7HoD$xmJ9nh" resolve="Exporter" />
    <node concept="1TJgyi" id="7HoD$xn6j2l" role="1TKVEl">
      <property role="TrG5h" value="fileName" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="7HoD$xn6j2n" role="1TKVEl">
      <property role="TrG5h" value="exportAll" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyi" id="7HoD$xn6j2o" role="1TKVEl">
      <property role="TrG5h" value="fileNme" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="bwQnMm_b8l">
    <property role="TrG5h" value="VariableRefExtension" />
    <ref role="1TJDcQ" to="tpee:fz7vLUo" resolve="VariableReference" />
  </node>
  <node concept="1TIwiD" id="74LOPGM6KOt">
    <property role="TrG5h" value="instanceMethodDeclarationEx" />
    <property role="34LRSv" value="add transfomers" />
    <property role="R4oN_" value="helps transform fields post init" />
    <ref role="1TJDcQ" to="tpee:fzclF8t" resolve="InstanceMethodDeclaration" />
  </node>
  <node concept="1TIwiD" id="4Iz7iG3JYy5">
    <property role="TrG5h" value="instanceMethodDeclarationExReference" />
    <property role="R4oN_" value="an instance method declaration" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="4Iz7iG3KgXr" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="type" />
      <property role="20lbJX" value="0..1" />
      <ref role="20lvS9" to="tpee:g7uibYu" resolve="ClassifierType" />
    </node>
    <node concept="1TJgyj" id="4Iz7iG3JZ1T" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="instMethod" />
      <ref role="20lvS9" node="74LOPGM6KOt" resolve="instanceMethodDeclarationEx" />
    </node>
  </node>
</model>

