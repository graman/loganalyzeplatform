<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:c85ac26b-cb3e-4cdf-af6f-2576db0114ce(LogScrapper.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="-1" />
    <use id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="80av" ref="r:304512bc-2ab8-4538-a01c-29a28f4fb5a1(LogScrapper.structure)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="tpek" ref="r:00000000-0000-4000-0000-011c895902c0(jetbrains.mps.baseLanguage.behavior)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" />
    <import index="tpeh" ref="r:00000000-0000-4000-0000-011c895902c5(jetbrains.mps.baseLanguage.typesystem)" />
    <import index="tp4f" ref="r:00000000-0000-4000-0000-011c89590373(jetbrains.mps.baseLanguage.classifiers.structure)" />
    <import index="e2lb" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.lang(JDK/java.lang@java_stub)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6" />
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1082113931046" name="jetbrains.mps.baseLanguage.structure.ContinueStatement" flags="nn" index="3N13vt" />
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="1196350785113" name="jetbrains.mps.lang.quotation.structure.Quotation" flags="nn" index="2c44tf">
        <child id="1196350785114" name="quotedNode" index="2c44tc" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1207055528241" name="jetbrains.mps.lang.typesystem.structure.WarningStatement" flags="nn" index="a7r0C">
        <child id="1207055552304" name="warningText" index="a7wSD" />
      </concept>
      <concept id="1185788614172" name="jetbrains.mps.lang.typesystem.structure.NormalTypeClause" flags="ng" index="mw_s8">
        <child id="1185788644032" name="normalType" index="mwGJk" />
      </concept>
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1227096774658" name="jetbrains.mps.lang.typesystem.structure.MessageStatement" flags="ng" index="2OEH$v">
        <child id="1227096802790" name="nodeToReport" index="2OEOjV" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <property id="1195213689297" name="overrides" index="18ip37" />
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174643105530" name="jetbrains.mps.lang.typesystem.structure.InferenceRule" flags="ig" index="1YbPZF" />
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
      <concept id="1174657487114" name="jetbrains.mps.lang.typesystem.structure.TypeOfExpression" flags="nn" index="1Z2H0r">
        <child id="1174657509053" name="term" index="1Z2MuG" />
      </concept>
      <concept id="1174658326157" name="jetbrains.mps.lang.typesystem.structure.CreateEquationStatement" flags="nn" index="1Z5TYs" />
      <concept id="1174660718586" name="jetbrains.mps.lang.typesystem.structure.AbstractEquationStatement" flags="nn" index="1Zf1VF">
        <property id="1206359757216" name="checkOnly" index="3wDh2S" />
        <child id="1174660783413" name="leftExpression" index="1ZfhK$" />
        <child id="1174660783414" name="rightExpression" index="1ZfhKB" />
      </concept>
      <concept id="1174663118805" name="jetbrains.mps.lang.typesystem.structure.CreateLessThanInequationStatement" flags="nn" index="1ZobV4" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1145383075378" name="jetbrains.mps.lang.smodel.structure.SNodeListType" flags="in" index="2I9FWS">
        <reference id="1145383142433" name="elementConcept" index="2I9WkF" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1171999116870" name="jetbrains.mps.lang.smodel.structure.Node_IsNullOperation" flags="nn" index="3w_OXm" />
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <reference id="1140138128738" name="concept" index="1PxNhF" />
        <child id="1140138123956" name="leftExpression" index="1PxMeX" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
      <concept id="1172424058054" name="jetbrains.mps.lang.smodel.structure.ConceptRefExpression" flags="nn" index="3TUQnm">
        <reference id="1172424100906" name="conceptDeclaration" index="3TV0OU" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435807" name="elementType" index="HW$YZ" />
      </concept>
      <concept id="1201306600024" name="jetbrains.mps.baseLanguage.collections.structure.ContainsKeyOperation" flags="nn" index="2Nt0df">
        <child id="1201654602639" name="key" index="38cxEo" />
      </concept>
      <concept id="1160600644654" name="jetbrains.mps.baseLanguage.collections.structure.ListCreatorWithInit" flags="nn" index="Tc6Ow" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1240216724530" name="jetbrains.mps.baseLanguage.collections.structure.LinkedHashMapCreator" flags="nn" index="32Fmki" />
      <concept id="1197683403723" name="jetbrains.mps.baseLanguage.collections.structure.MapType" flags="in" index="3rvAFt">
        <child id="1197683466920" name="keyType" index="3rvQeY" />
        <child id="1197683475734" name="valueType" index="3rvSg0" />
      </concept>
      <concept id="1197686869805" name="jetbrains.mps.baseLanguage.collections.structure.HashMapCreator" flags="nn" index="3rGOSV">
        <child id="1197687026896" name="keyType" index="3rHrn6" />
        <child id="1197687035757" name="valueType" index="3rHtpV" />
      </concept>
      <concept id="1197932370469" name="jetbrains.mps.baseLanguage.collections.structure.MapElement" flags="nn" index="3EllGN">
        <child id="1197932505799" name="map" index="3ElQJh" />
        <child id="1197932525128" name="key" index="3ElVtu" />
      </concept>
      <concept id="1172254888721" name="jetbrains.mps.baseLanguage.collections.structure.ContainsOperation" flags="nn" index="3JPx81" />
    </language>
  </registry>
  <node concept="18kY7G" id="7HoD$xmMXMI">
    <property role="TrG5h" value="check_Exporter" />
    <node concept="3clFbS" id="7HoD$xmMXMJ" role="18ibNy">
      <node concept="3cpWs8" id="7HoD$xmMXZD" role="3cqZAp">
        <node concept="3cpWsn" id="7HoD$xmMXZG" role="3cpWs9">
          <property role="TrG5h" value="giCount" />
          <node concept="3rvAFt" id="7HoD$xmMXZz" role="1tU5fm">
            <node concept="17QB3L" id="7HoD$xmMY1s" role="3rvQeY" />
            <node concept="10Oyi0" id="7HoD$xmMY0X" role="3rvSg0" />
          </node>
          <node concept="2ShNRf" id="7HoD$xmMYgE" role="33vP2m">
            <node concept="32Fmki" id="7HoD$xmMYg$" role="2ShVmc">
              <node concept="17QB3L" id="7HoD$xmMYg_" role="3rHrn6" />
              <node concept="10Oyi0" id="7HoD$xmMYgA" role="3rHtpV" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2Gpval" id="7HoD$xmMXNl" role="3cqZAp">
        <node concept="2GrKxI" id="7HoD$xmMXNn" role="2Gsz3X">
          <property role="TrG5h" value="grpItemRef" />
        </node>
        <node concept="3clFbS" id="7HoD$xmMXNp" role="2LFqv$">
          <node concept="3cpWs8" id="7HoD$xmQ2o8" role="3cqZAp">
            <node concept="3cpWsn" id="7HoD$xmQ2o9" role="3cpWs9">
              <property role="TrG5h" value="groupItem" />
              <node concept="3Tqbb2" id="7HoD$xmQ2ne" role="1tU5fm">
                <ref role="ehGHo" to="80av:1ZzbrikhPdP" resolve="GroupItem" />
              </node>
              <node concept="2OqwBi" id="7HoD$xmQ2oa" role="33vP2m">
                <node concept="2GrUjf" id="7HoD$xmQ2ob" role="2Oq$k0">
                  <ref role="2Gs0qQ" node="7HoD$xmMXNn" resolve="grpItemRef" />
                </node>
                <node concept="3TrEf2" id="7HoD$xmQ2oc" role="2OqNvi">
                  <ref role="3Tt5mk" to="80av:7HoD$xmMerN" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbJ" id="7HoD$xmQ2ys" role="3cqZAp">
            <node concept="3clFbS" id="7HoD$xmQ2yu" role="3clFbx">
              <node concept="3N13vt" id="7HoD$xmQ2CQ" role="3cqZAp" />
            </node>
            <node concept="3clFbC" id="7HoD$xmQ2Cl" role="3clFbw">
              <node concept="10Nm6u" id="7HoD$xmQ2CA" role="3uHU7w" />
              <node concept="37vLTw" id="7HoD$xmQ2AA" role="3uHU7B">
                <ref role="3cqZAo" node="7HoD$xmQ2o9" resolve="groupItem" />
              </node>
            </node>
          </node>
          <node concept="3clFbJ" id="7HoD$xmMYhs" role="3cqZAp">
            <node concept="3clFbS" id="7HoD$xmMYht" role="3clFbx">
              <node concept="2MkqsV" id="7HoD$xmQdbz" role="3cqZAp">
                <node concept="2GrUjf" id="7HoD$xmQe4a" role="2OEOjV">
                  <ref role="2Gs0qQ" node="7HoD$xmMXNn" resolve="grpItemRef" />
                </node>
                <node concept="Xl_RD" id="7HoD$xmQdbP" role="2MkJ7o">
                  <property role="Xl_RC" value="An exporter contains only one member of one name" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="7HoD$xmMYwl" role="3clFbw">
              <node concept="37vLTw" id="7HoD$xmMYhC" role="2Oq$k0">
                <ref role="3cqZAo" node="7HoD$xmMXZG" resolve="giCount" />
              </node>
              <node concept="2Nt0df" id="7HoD$xmN11K" role="2OqNvi">
                <node concept="2OqwBi" id="7HoD$xmN1s3" role="38cxEo">
                  <node concept="37vLTw" id="7HoD$xmQ2oe" role="2Oq$k0">
                    <ref role="3cqZAo" node="7HoD$xmQ2o9" resolve="groupItem" />
                  </node>
                  <node concept="3TrcHB" id="7HoD$xmN1AO" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="7HoD$xmQaZY" role="3cqZAp">
            <node concept="37vLTI" id="7HoD$xmQd2B" role="3clFbG">
              <node concept="3cmrfG" id="7HoD$xmQd9E" role="37vLTx">
                <property role="3cmrfH" value="0" />
              </node>
              <node concept="3EllGN" id="7HoD$xmQbbv" role="37vLTJ">
                <node concept="2OqwBi" id="7HoD$xmQclV" role="3ElVtu">
                  <node concept="37vLTw" id="7HoD$xmQcjN" role="2Oq$k0">
                    <ref role="3cqZAo" node="7HoD$xmQ2o9" resolve="groupItem" />
                  </node>
                  <node concept="3TrcHB" id="7HoD$xmQczC" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
                <node concept="37vLTw" id="7HoD$xmQaZW" role="3ElQJh">
                  <ref role="3cqZAo" node="7HoD$xmMXZG" resolve="giCount" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="7HoD$xmMXQ2" role="2GsD0m">
          <node concept="1YBJjd" id="7HoD$xmMXOt" role="2Oq$k0">
            <ref role="1YBMHb" node="7HoD$xmMXML" resolve="exporter" />
          </node>
          <node concept="3Tsc0h" id="7HoD$xmMXXD" role="2OqNvi">
            <ref role="3TtcxE" to="80av:7HoD$xmMesq" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="7HoD$xmMXML" role="1YuTPh">
      <property role="TrG5h" value="exporter" />
      <ref role="1YaFvo" to="80av:7HoD$xmJ9nh" resolve="Exporter" />
    </node>
  </node>
  <node concept="1YbPZF" id="74LOPGLYh0k">
    <property role="TrG5h" value="typeof_VariableRefExtension" />
    <node concept="3clFbS" id="74LOPGLYh0l" role="18ibNy">
      <node concept="3clFbJ" id="74LOPGLYhgq" role="3cqZAp">
        <node concept="3clFbS" id="74LOPGLYhgr" role="3clFbx">
          <node concept="1Z5TYs" id="74LOPGLYj44" role="3cqZAp">
            <node concept="mw_s8" id="74LOPGLYj4m" role="1ZfhKB">
              <node concept="2OqwBi" id="74LOPGLYjLm" role="mwGJk">
                <node concept="2OqwBi" id="74LOPGLYj7x" role="2Oq$k0">
                  <node concept="1YBJjd" id="74LOPGLYj4k" role="2Oq$k0">
                    <ref role="1YBMHb" node="74LOPGLYh0n" resolve="variableRefExtension" />
                  </node>
                  <node concept="3TrEf2" id="74LOPGLYjsC" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
                  </node>
                </node>
                <node concept="3TrEf2" id="74LOPGLYkci" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:4VkOLwjf83e" />
                </node>
              </node>
            </node>
            <node concept="mw_s8" id="74LOPGLYj47" role="1ZfhK$">
              <node concept="1Z2H0r" id="74LOPGLYj1E" role="mwGJk">
                <node concept="1YBJjd" id="74LOPGLYj26" role="1Z2MuG">
                  <ref role="1YBMHb" node="74LOPGLYh0n" resolve="variableRefExtension" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs6" id="74LOPGLYkTq" role="3cqZAp" />
        </node>
        <node concept="2OqwBi" id="74LOPGLYiew" role="3clFbw">
          <node concept="2OqwBi" id="74LOPGLYhk6" role="2Oq$k0">
            <node concept="1YBJjd" id="74LOPGLYhgA" role="2Oq$k0">
              <ref role="1YBMHb" node="74LOPGLYh0n" resolve="variableRefExtension" />
            </node>
            <node concept="3TrEf2" id="74LOPGLYhSx" role="2OqNvi">
              <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
            </node>
          </node>
          <node concept="1mIQ4w" id="74LOPGLYiUP" role="2OqNvi">
            <node concept="chp4Y" id="74LOPGLYiW_" role="cj9EA">
              <ref role="cht4Q" to="80av:66tNEy7MCIO" resolve="Member" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="74LOPGM1FN1" role="3cqZAp" />
      <node concept="3clFbJ" id="wqExlZcjHe" role="3cqZAp">
        <node concept="3clFbS" id="wqExlZcjHf" role="3clFbx">
          <node concept="3cpWs8" id="2eR5sdQxkxH" role="3cqZAp">
            <node concept="3cpWsn" id="2eR5sdQxkxI" role="3cpWs9">
              <property role="TrG5h" value="fieldDecl" />
              <property role="3TUv4t" value="true" />
              <node concept="3Tqbb2" id="2eR5sdQxkxJ" role="1tU5fm">
                <ref role="ehGHo" to="tpee:fz12cDC" resolve="FieldDeclaration" />
              </node>
              <node concept="1PxgMI" id="2eR5sdQxkxK" role="33vP2m">
                <ref role="1PxNhF" to="tpee:fz12cDC" resolve="FieldDeclaration" />
                <node concept="2OqwBi" id="2eR5sdQxkxL" role="1PxMeX">
                  <node concept="1YBJjd" id="74LOPGLYlch" role="2Oq$k0">
                    <ref role="1YBMHb" node="74LOPGLYh0n" resolve="variableRefExtension" />
                  </node>
                  <node concept="3TrEf2" id="2eR5sdQxkxN" role="2OqNvi">
                    <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbJ" id="2eR5sdQxkxO" role="3cqZAp">
            <node concept="3clFbS" id="2eR5sdQxkxP" role="3clFbx">
              <node concept="3cpWs6" id="2eR5sdQxkxQ" role="3cqZAp" />
            </node>
            <node concept="2OqwBi" id="2eR5sdQxkxR" role="3clFbw">
              <node concept="37vLTw" id="3GM_nagTvZH" role="2Oq$k0">
                <ref role="3cqZAo" node="2eR5sdQxkxI" resolve="fieldDecl" />
              </node>
              <node concept="3w_OXm" id="2eR5sdQxkxT" role="2OqNvi" />
            </node>
          </node>
          <node concept="3clFbH" id="2eR5sdQxkxU" role="3cqZAp" />
          <node concept="3cpWs8" id="wfLGLyj6Tq" role="3cqZAp">
            <node concept="3cpWsn" id="wfLGLyj6Tr" role="3cpWs9">
              <property role="TrG5h" value="contextClassifier" />
              <node concept="2OqwBi" id="L_Hr3kExIZ" role="33vP2m">
                <node concept="2qgKlT" id="L_Hr3kExJ0" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:5mDmeD1aaq0" resolve="getContextClassifier" />
                  <node concept="1YBJjd" id="74LOPGLYlz2" role="37wK5m">
                    <ref role="1YBMHb" node="74LOPGLYh0n" resolve="variableRefExtension" />
                  </node>
                </node>
                <node concept="3TUQnm" id="L_Hr3kExJ2" role="2Oq$k0">
                  <ref role="3TV0OU" to="tpee:g7pOWCK" resolve="Classifier" />
                </node>
              </node>
              <node concept="3Tqbb2" id="wfLGLyj6Ts" role="1tU5fm">
                <ref role="ehGHo" to="tpee:g7pOWCK" resolve="Classifier" />
              </node>
            </node>
          </node>
          <node concept="3clFbJ" id="wfLGLyj7KB" role="3cqZAp">
            <node concept="3clFbS" id="wfLGLyj7KC" role="3clFbx">
              <node concept="3cpWs6" id="wfLGLyj7L4" role="3cqZAp" />
            </node>
            <node concept="3clFbC" id="wfLGLyj7L0" role="3clFbw">
              <node concept="37vLTw" id="3GM_nagT$xu" role="3uHU7B">
                <ref role="3cqZAo" node="wfLGLyj6Tr" resolve="contextClassifier" />
              </node>
              <node concept="10Nm6u" id="wfLGLyj7L3" role="3uHU7w" />
            </node>
          </node>
          <node concept="3clFbH" id="wfLGLyj7L6" role="3cqZAp" />
          <node concept="3cpWs8" id="wfLGLyj7Kt" role="3cqZAp">
            <node concept="3cpWsn" id="wfLGLyj7Ku" role="3cpWs9">
              <property role="TrG5h" value="thisType" />
              <node concept="3Tqbb2" id="wfLGLyj7Kv" role="1tU5fm">
                <ref role="ehGHo" to="tpee:g7uibYu" resolve="ClassifierType" />
              </node>
              <node concept="2OqwBi" id="wfLGLyj7Kw" role="33vP2m">
                <node concept="37vLTw" id="3GM_nagTBgL" role="2Oq$k0">
                  <ref role="3cqZAo" node="wfLGLyj6Tr" resolve="contextClassifier" />
                </node>
                <node concept="2qgKlT" id="wfLGLyj7Ky" role="2OqNvi">
                  <ref role="37wK5l" to="tpek:2RtWPFZ12w7" resolve="getThisType" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWs8" id="wfLGLyj7Mb" role="3cqZAp">
            <node concept="3cpWsn" id="wfLGLyj7Mc" role="3cpWs9">
              <property role="TrG5h" value="fieldType" />
              <node concept="3Tqbb2" id="wfLGLyj7Md" role="1tU5fm" />
              <node concept="2OqwBi" id="wfLGLyj7Me" role="33vP2m">
                <node concept="37vLTw" id="2eR5sdQxkxX" role="2Oq$k0">
                  <ref role="3cqZAo" node="2eR5sdQxkxI" resolve="fieldDecl" />
                </node>
                <node concept="3TrEf2" id="wfLGLyj7Mg" role="2OqNvi">
                  <ref role="3Tt5mk" to="tpee:4VkOLwjf83e" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbJ" id="wfLGLyj7Mj" role="3cqZAp">
            <node concept="3clFbS" id="wfLGLyj7Mk" role="3clFbx">
              <node concept="3cpWs8" id="wfLGLyj7L8" role="3cqZAp">
                <node concept="3cpWsn" id="wfLGLyj7L9" role="3cpWs9">
                  <property role="TrG5h" value="subs" />
                  <node concept="3rvAFt" id="wfLGLyj7La" role="1tU5fm">
                    <node concept="3Tqbb2" id="wfLGLyj7Lb" role="3rvQeY" />
                    <node concept="3Tqbb2" id="wfLGLyj7Lc" role="3rvSg0" />
                  </node>
                  <node concept="2ShNRf" id="wfLGLyj7Ld" role="33vP2m">
                    <node concept="3rGOSV" id="wfLGLyj7Le" role="2ShVmc">
                      <node concept="3Tqbb2" id="wfLGLyj7Lf" role="3rHrn6" />
                      <node concept="3Tqbb2" id="wfLGLyj7Lg" role="3rHtpV" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="wfLGLyj7Li" role="3cqZAp">
                <node concept="2OqwBi" id="wfLGLyj7LC" role="3clFbG">
                  <node concept="37vLTw" id="3GM_nagTwlw" role="2Oq$k0">
                    <ref role="3cqZAo" node="wfLGLyj7Ku" resolve="thisType" />
                  </node>
                  <node concept="2qgKlT" id="wfLGLyj7LH" role="2OqNvi">
                    <ref role="37wK5l" to="tpek:3zZky3wF74h" resolve="collectGenericSubstitutions" />
                    <node concept="37vLTw" id="3GM_nagTvJ8" role="37wK5m">
                      <ref role="3cqZAo" node="wfLGLyj7L9" resolve="subs" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="wfLGLyj7MO" role="3cqZAp">
                <node concept="2OqwBi" id="wfLGLyj7NU" role="3clFbG">
                  <node concept="1PxgMI" id="wfLGLyj7N$" role="2Oq$k0">
                    <ref role="1PxNhF" to="tpee:3zZky3wF74d" resolve="IGenericType" />
                    <node concept="37vLTw" id="3GM_nagTz68" role="1PxMeX">
                      <ref role="3cqZAo" node="wfLGLyj7Mc" resolve="fieldType" />
                    </node>
                  </node>
                  <node concept="2qgKlT" id="wfLGLyj7NZ" role="2OqNvi">
                    <ref role="37wK5l" to="tpek:3zZky3wF74h" resolve="collectGenericSubstitutions" />
                    <node concept="37vLTw" id="3GM_nagTzvz" role="37wK5m">
                      <ref role="3cqZAo" node="wfLGLyj7L9" resolve="subs" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="wfLGLyj7O2" role="3cqZAp">
                <node concept="37vLTI" id="wfLGLyj7Po" role="3clFbG">
                  <node concept="37vLTw" id="3GM_nagTBan" role="37vLTJ">
                    <ref role="3cqZAo" node="wfLGLyj7Mc" resolve="fieldType" />
                  </node>
                  <node concept="2OqwBi" id="wfLGLyj7P8" role="37vLTx">
                    <node concept="1PxgMI" id="wfLGLyj7OM" role="2Oq$k0">
                      <ref role="1PxNhF" to="tpee:3zZky3wF74d" resolve="IGenericType" />
                      <node concept="37vLTw" id="3GM_nagT$1Q" role="1PxMeX">
                        <ref role="3cqZAo" node="wfLGLyj7Mc" resolve="fieldType" />
                      </node>
                    </node>
                    <node concept="2qgKlT" id="wfLGLyj7Pd" role="2OqNvi">
                      <ref role="37wK5l" to="tpek:3zZky3wFPhu" resolve="expandGenerics" />
                      <node concept="37vLTw" id="3GM_nagTAxj" role="37wK5m">
                        <ref role="3cqZAo" node="wfLGLyj7L9" resolve="subs" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="wfLGLyj7MG" role="3clFbw">
              <node concept="37vLTw" id="3GM_nagTx6h" role="2Oq$k0">
                <ref role="3cqZAo" node="wfLGLyj7Mc" resolve="fieldType" />
              </node>
              <node concept="1mIQ4w" id="wfLGLyj7ML" role="2OqNvi">
                <node concept="chp4Y" id="wfLGLyj7MN" role="cj9EA">
                  <ref role="cht4Q" to="tpee:3zZky3wF74d" resolve="IGenericType" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1Z5TYs" id="wfLGLyj7PZ" role="3cqZAp">
            <node concept="mw_s8" id="wfLGLyj7Q3" role="1ZfhKB">
              <node concept="37vLTw" id="3GM_nagTvzO" role="mwGJk">
                <ref role="3cqZAo" node="wfLGLyj7Mc" resolve="fieldType" />
              </node>
            </node>
            <node concept="mw_s8" id="wfLGLyj7Q2" role="1ZfhK$">
              <node concept="1Z2H0r" id="wfLGLyj7PC" role="mwGJk">
                <node concept="1YBJjd" id="74LOPGLYlTD" role="1Z2MuG">
                  <ref role="1YBMHb" node="74LOPGLYh0n" resolve="variableRefExtension" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1ZobV4" id="2eR5sdQyHWh" role="3cqZAp">
            <property role="3wDh2S" value="true" />
            <node concept="mw_s8" id="2eR5sdQyHWi" role="1ZfhK$">
              <node concept="1Z2H0r" id="2eR5sdQyHWj" role="mwGJk">
                <node concept="1YBJjd" id="74LOPGLYmg1" role="1Z2MuG">
                  <ref role="1YBMHb" node="74LOPGLYh0n" resolve="variableRefExtension" />
                </node>
              </node>
            </node>
            <node concept="mw_s8" id="2eR5sdQyHWl" role="1ZfhKB">
              <node concept="2c44tf" id="2eR5sdQyHWm" role="mwGJk">
                <node concept="3uibUv" id="pypzLMmGh6" role="2c44tc">
                  <ref role="3uigEE" to="e2lb:~Object" resolve="Object" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbH" id="wqExlZcjI$" role="3cqZAp" />
          <node concept="3cpWs6" id="wqExlZcjIA" role="3cqZAp" />
        </node>
        <node concept="2OqwBi" id="wqExlZcjIm" role="3clFbw">
          <node concept="2OqwBi" id="wqExlZcjHV" role="2Oq$k0">
            <node concept="1YBJjd" id="74LOPGLYkWg" role="2Oq$k0">
              <ref role="1YBMHb" node="74LOPGLYh0n" resolve="variableRefExtension" />
            </node>
            <node concept="3TrEf2" id="wqExlZcjI0" role="2OqNvi">
              <ref role="3Tt5mk" to="tpee:fzcqZ_w" />
            </node>
          </node>
          <node concept="1mIQ4w" id="wqExlZcjIs" role="2OqNvi">
            <node concept="chp4Y" id="wqExlZcjIu" role="cj9EA">
              <ref role="cht4Q" to="tpee:fz12cDC" resolve="FieldDeclaration" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="74LOPGLYkjr" role="3cqZAp" />
    </node>
    <node concept="1YaCAy" id="74LOPGLYh0n" role="1YuTPh">
      <property role="TrG5h" value="variableRefExtension" />
      <ref role="1YaFvo" to="80av:bwQnMm_b8l" resolve="VariableRefExtension" />
    </node>
  </node>
  <node concept="1YbPZF" id="74LOPGLZTPX">
    <property role="TrG5h" value="typeof_LoggedItem" />
    <node concept="3clFbS" id="74LOPGLZTPY" role="18ibNy">
      <node concept="3cpWs8" id="74LOPGLZTT9" role="3cqZAp">
        <node concept="3cpWsn" id="74LOPGLZTTc" role="3cpWs9">
          <property role="TrG5h" value="clsConcept" />
          <node concept="3Tqbb2" id="74LOPGLZTT8" role="1tU5fm">
            <ref role="ehGHo" to="tpee:fz12cDA" resolve="ClassConcept" />
          </node>
          <node concept="2ShNRf" id="74LOPGLZTTF" role="33vP2m">
            <node concept="3zrR0B" id="74LOPGLZTTD" role="2ShVmc">
              <node concept="3Tqbb2" id="74LOPGLZTTE" role="3zrR0E">
                <ref role="ehGHo" to="tpee:fz12cDA" resolve="ClassConcept" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbF" id="74LOPGLZTU2" role="3cqZAp">
        <node concept="37vLTI" id="74LOPGLZVq7" role="3clFbG">
          <node concept="2OqwBi" id="74LOPGLZVx6" role="37vLTx">
            <node concept="1YBJjd" id="74LOPGLZVv9" role="2Oq$k0">
              <ref role="1YBMHb" node="74LOPGLZTQ0" resolve="loggedItem" />
            </node>
            <node concept="3TrcHB" id="74LOPGLZVGM" role="2OqNvi">
              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
            </node>
          </node>
          <node concept="2OqwBi" id="74LOPGLZU32" role="37vLTJ">
            <node concept="37vLTw" id="74LOPGLZTU0" role="2Oq$k0">
              <ref role="3cqZAo" node="74LOPGLZTTc" resolve="clsConcept" />
            </node>
            <node concept="3TrcHB" id="74LOPGLZUDe" role="2OqNvi">
              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbF" id="74LOPGM11v$" role="3cqZAp">
        <node concept="37vLTI" id="74LOPGM12On" role="3clFbG">
          <node concept="2ShNRf" id="74LOPGM12R3" role="37vLTx">
            <node concept="3zrR0B" id="74LOPGM13hr" role="2ShVmc">
              <node concept="3Tqbb2" id="74LOPGM13ht" role="3zrR0E">
                <ref role="ehGHo" to="tpee:gFTm1ZL" resolve="PublicVisibility" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="74LOPGM11B8" role="37vLTJ">
            <node concept="37vLTw" id="74LOPGM11vy" role="2Oq$k0">
              <ref role="3cqZAo" node="74LOPGLZTTc" resolve="clsConcept" />
            </node>
            <node concept="3TrEf2" id="74LOPGM12dk" role="2OqNvi">
              <ref role="3Tt5mk" to="tpee:h9B3oxE" />
            </node>
          </node>
        </node>
      </node>
      <node concept="1Z5TYs" id="74LOPGLZWnX" role="3cqZAp">
        <node concept="mw_s8" id="74LOPGLZWrW" role="1ZfhKB">
          <node concept="37vLTw" id="74LOPGLZWrU" role="mwGJk">
            <ref role="3cqZAo" node="74LOPGLZTTc" resolve="clsConcept" />
          </node>
        </node>
        <node concept="mw_s8" id="74LOPGLZWo0" role="1ZfhK$">
          <node concept="1Z2H0r" id="74LOPGLZWh0" role="mwGJk">
            <node concept="1YBJjd" id="74LOPGLZWm2" role="1Z2MuG">
              <ref role="1YBMHb" node="74LOPGLZTQ0" resolve="loggedItem" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="74LOPGLZTQ0" role="1YuTPh">
      <property role="TrG5h" value="loggedItem" />
      <ref role="1YaFvo" to="80av:66tNEy7Mgmx" resolve="LoggedItem" />
    </node>
  </node>
  <node concept="18kY7G" id="74LOPGM3Saw">
    <property role="TrG5h" value="checkForMembers" />
    <node concept="3clFbS" id="74LOPGM3Sax" role="18ibNy">
      <node concept="3cpWs8" id="74LOPGM47Ra" role="3cqZAp">
        <node concept="3cpWsn" id="74LOPGM47Rb" role="3cpWs9">
          <property role="TrG5h" value="expressions" />
          <node concept="2I9FWS" id="74LOPGM47R5" role="1tU5fm">
            <ref role="2I9WkF" to="80av:1ZzbrikhPdP" resolve="GroupItem" />
          </node>
          <node concept="2OqwBi" id="74LOPGM47Rc" role="33vP2m">
            <node concept="2OqwBi" id="74LOPGM47Rd" role="2Oq$k0">
              <node concept="1YBJjd" id="74LOPGM47Re" role="2Oq$k0">
                <ref role="1YBMHb" node="74LOPGM3Sb7" resolve="loggedItem" />
              </node>
              <node concept="3TrEf2" id="74LOPGM47Rf" role="2OqNvi">
                <ref role="3Tt5mk" to="80av:1Zzbrikhzf4" />
              </node>
            </node>
            <node concept="3Tsc0h" id="74LOPGM47Rg" role="2OqNvi">
              <ref role="3TtcxE" to="80av:1Zzbrikhxyq" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3cpWs8" id="74LOPGM47XC" role="3cqZAp">
        <node concept="3cpWsn" id="74LOPGM47XF" role="3cpWs9">
          <property role="TrG5h" value="names" />
          <node concept="_YKpA" id="74LOPGM49U_" role="1tU5fm">
            <node concept="17QB3L" id="74LOPGM49UB" role="_ZDj9" />
          </node>
          <node concept="2ShNRf" id="74LOPGM4atO" role="33vP2m">
            <node concept="Tc6Ow" id="74LOPGM4atf" role="2ShVmc">
              <node concept="17QB3L" id="74LOPGM4atg" role="HW$YZ" />
            </node>
          </node>
        </node>
      </node>
      <node concept="2Gpval" id="74LOPGM47Zp" role="3cqZAp">
        <node concept="2GrKxI" id="74LOPGM47Zr" role="2Gsz3X">
          <property role="TrG5h" value="grpItem" />
        </node>
        <node concept="3clFbS" id="74LOPGM47Zt" role="2LFqv$">
          <node concept="3clFbJ" id="74LOPGM480W" role="3cqZAp">
            <node concept="3clFbS" id="74LOPGM480X" role="3clFbx">
              <node concept="2MkqsV" id="74LOPGM48Vh" role="3cqZAp">
                <node concept="2GrUjf" id="74LOPGM48Zf" role="2OEOjV">
                  <ref role="2Gs0qQ" node="74LOPGM47Zr" resolve="grpItem" />
                </node>
                <node concept="Xl_RD" id="74LOPGM48Vt" role="2MkJ7o">
                  <property role="Xl_RC" value="Two members cannot share the same name" />
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="74LOPGM48ax" role="3clFbw">
              <node concept="37vLTw" id="74LOPGM4818" role="2Oq$k0">
                <ref role="3cqZAo" node="74LOPGM47XF" resolve="names" />
              </node>
              <node concept="3JPx81" id="74LOPGM48vT" role="2OqNvi">
                <node concept="2OqwBi" id="74LOPGM48yt" role="25WWJ7">
                  <node concept="2GrUjf" id="74LOPGM48wm" role="2Oq$k0">
                    <ref role="2Gs0qQ" node="74LOPGM47Zr" resolve="grpItem" />
                  </node>
                  <node concept="3TrcHB" id="74LOPGM48Tm" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbF" id="74LOPGM4a_V" role="3cqZAp">
            <node concept="2OqwBi" id="74LOPGM4aQd" role="3clFbG">
              <node concept="37vLTw" id="74LOPGM4a_T" role="2Oq$k0">
                <ref role="3cqZAo" node="74LOPGM47XF" resolve="names" />
              </node>
              <node concept="TSZUe" id="74LOPGM4bAf" role="2OqNvi">
                <node concept="2OqwBi" id="74LOPGM4bGy" role="25WWJ7">
                  <node concept="2GrUjf" id="74LOPGM4bDJ" role="2Oq$k0">
                    <ref role="2Gs0qQ" node="74LOPGM47Zr" resolve="grpItem" />
                  </node>
                  <node concept="3TrcHB" id="74LOPGM4bWW" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="37vLTw" id="74LOPGM480$" role="2GsD0m">
          <ref role="3cqZAo" node="74LOPGM47Rb" resolve="expressions" />
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="74LOPGM3Sb7" role="1YuTPh">
      <property role="TrG5h" value="loggedItem" />
      <ref role="1YaFvo" to="80av:66tNEy7Mgmx" resolve="LoggedItem" />
    </node>
  </node>
  <node concept="1YbPZF" id="74LOPGM4edD">
    <property role="TrG5h" value="typeof_Member" />
    <property role="18ip37" value="true" />
    <node concept="3clFbS" id="74LOPGM4edE" role="18ibNy">
      <node concept="1Z5TYs" id="74LOPGM4egV" role="3cqZAp">
        <node concept="mw_s8" id="74LOPGM4ehd" role="1ZfhKB">
          <node concept="2OqwBi" id="74LOPGM4epd" role="mwGJk">
            <node concept="1YBJjd" id="74LOPGM4ehb" role="2Oq$k0">
              <ref role="1YBMHb" node="74LOPGM4eeg" resolve="member" />
            </node>
            <node concept="3TrEf2" id="74LOPGM4f1U" role="2OqNvi">
              <ref role="3Tt5mk" to="tpee:4VkOLwjf83e" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="74LOPGM4egY" role="1ZfhK$">
          <node concept="1Z2H0r" id="74LOPGM4eep" role="mwGJk">
            <node concept="1YBJjd" id="74LOPGM4eeP" role="1Z2MuG">
              <ref role="1YBMHb" node="74LOPGM4eeg" resolve="member" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="74LOPGM4eeg" role="1YuTPh">
      <property role="TrG5h" value="member" />
      <ref role="1YaFvo" to="80av:66tNEy7MCIO" resolve="Member" />
    </node>
  </node>
  <node concept="18kY7G" id="4Iz7iG3Zl2v">
    <property role="TrG5h" value="check_Pattern" />
    <node concept="3clFbS" id="4Iz7iG3Zl2w" role="18ibNy">
      <node concept="3clFbJ" id="4Iz7iG3ZleC" role="3cqZAp">
        <node concept="3clFbS" id="4Iz7iG3ZleD" role="3clFbx">
          <node concept="a7r0C" id="4Iz7iG3ZlK_" role="3cqZAp">
            <node concept="Xl_RD" id="4Iz7iG3ZlKR" role="a7wSD">
              <property role="Xl_RC" value="An identifier is a must" />
            </node>
            <node concept="1YBJjd" id="4Iz7iG3ZlOb" role="2OEOjV">
              <ref role="1YBMHb" node="4Iz7iG3Zl2y" resolve="pattern" />
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="4Iz7iG3Zlx7" role="3clFbw">
          <node concept="2OqwBi" id="4Iz7iG3Zlgd" role="2Oq$k0">
            <node concept="1YBJjd" id="4Iz7iG3ZleO" role="2Oq$k0">
              <ref role="1YBMHb" node="4Iz7iG3Zl2y" resolve="pattern" />
            </node>
            <node concept="3TrEf2" id="4Iz7iG3ZlnO" role="2OqNvi">
              <ref role="3Tt5mk" to="80av:_IX99AAH9S" />
            </node>
          </node>
          <node concept="3w_OXm" id="4Iz7iG3ZlK3" role="2OqNvi" />
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4Iz7iG3Zl2y" role="1YuTPh">
      <property role="TrG5h" value="pattern" />
      <ref role="1YaFvo" to="80av:1ZzbrikhxxY" resolve="Pattern" />
    </node>
  </node>
  <node concept="18kY7G" id="4Iz7iG3ZWi5">
    <property role="TrG5h" value="overrideBaseLangFieldRule" />
    <property role="18ip37" value="true" />
    <node concept="3clFbS" id="4Iz7iG3ZWi6" role="18ibNy" />
    <node concept="1YaCAy" id="4Iz7iG3ZWi8" role="1YuTPh">
      <property role="TrG5h" value="member" />
      <ref role="1YaFvo" to="80av:66tNEy7MCIO" resolve="Member" />
    </node>
  </node>
</model>

