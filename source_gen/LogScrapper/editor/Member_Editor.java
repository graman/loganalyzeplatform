package LogScrapper.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.DefaultNodeEditor;
import jetbrains.mps.openapi.editor.cells.EditorCell;
import jetbrains.mps.openapi.editor.EditorContext;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.nodeEditor.cells.EditorCell_Collection;
import jetbrains.mps.openapi.editor.style.Style;
import jetbrains.mps.editor.runtime.style.StyleImpl;
import jetbrains.mps.editor.runtime.style.StyleAttributes;
import jetbrains.mps.nodeEditor.cellProviders.CellProviderWithRole;
import jetbrains.mps.lang.editor.cellProviders.PropertyCellProvider;
import jetbrains.mps.nodeEditor.EditorManager;
import jetbrains.mps.nodeEditor.cells.EditorCell_Constant;
import jetbrains.mps.baseLanguage.editor.BaseLanguageStyle_StyleSheet;
import jetbrains.mps.lang.editor.cellProviders.RefNodeCellProvider;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SPropertyOperations;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.openapi.editor.style.StyleRegistry;
import LogScrapper.behavior.Member_Behavior;

public class Member_Editor extends DefaultNodeEditor {
  public EditorCell createEditorCell(EditorContext editorContext, SNode node) {
    return this.createCollection_jpw21l_a(editorContext, node);
  }
  public EditorCell createInspectedCell(EditorContext editorContext, SNode node) {
    return this.createCollection_jpw21l_a_0(editorContext, node);
  }
  private EditorCell createCollection_jpw21l_a(EditorContext editorContext, SNode node) {
    EditorCell_Collection editorCell = EditorCell_Collection.createVertical(editorContext, node);
    editorCell.setCellId("Collection_jpw21l_a");
    editorCell.setBig(true);
    editorCell.addEditorCell(this.createCollection_jpw21l_a0(editorContext, node));
    if (renderingCondition_jpw21l_a1a(node, editorContext)) {
      editorCell.addEditorCell(this.createConstant_jpw21l_b0(editorContext, node));
    }
    if (renderingCondition_jpw21l_a2a(node, editorContext)) {
      editorCell.addEditorCell(this.createRefNode_jpw21l_c0(editorContext, node));
    }
    return editorCell;
  }
  private EditorCell createCollection_jpw21l_a0(EditorContext editorContext, SNode node) {
    EditorCell_Collection editorCell = EditorCell_Collection.createHorizontal(editorContext, node);
    editorCell.setCellId("Collection_jpw21l_a0");
    Style style = new StyleImpl();
    style.set(StyleAttributes.RT_ANCHOR_TAG, 0, "default_RTransform");
    editorCell.getStyle().putAll(style);
    editorCell.addEditorCell(this.createProperty_jpw21l_a0a(editorContext, node));
    editorCell.addEditorCell(this.createConstant_jpw21l_b0a(editorContext, node));
    editorCell.addEditorCell(this.createConstant_jpw21l_c0a(editorContext, node));
    editorCell.addEditorCell(this.createRefNode_jpw21l_d0a(editorContext, node));
    editorCell.addEditorCell(this.createConstant_jpw21l_e0a(editorContext, node));
    editorCell.addEditorCell(this.createConstant_jpw21l_f0a(editorContext, node));
    editorCell.addEditorCell(this.createProperty_jpw21l_g0a(editorContext, node));
    editorCell.addEditorCell(this.createConstant_jpw21l_h0a(editorContext, node));
    if (renderingCondition_jpw21l_a8a0(node, editorContext)) {
      editorCell.addEditorCell(this.createConstant_jpw21l_i0a(editorContext, node));
    }
    if (renderingCondition_jpw21l_a9a0(node, editorContext)) {
      editorCell.addEditorCell(this.createProperty_jpw21l_j0a(editorContext, node));
    }
    return editorCell;
  }
  private EditorCell createProperty_jpw21l_a0a(EditorContext editorContext, SNode node) {
    CellProviderWithRole provider = new PropertyCellProvider(node, editorContext);
    provider.setRole("name");
    provider.setNoTargetText("<no name>");
    EditorCell editorCell;
    editorCell = provider.createEditorCell(editorContext);
    editorCell.setCellId("property_name");
    editorCell.setSubstituteInfo(provider.createDefaultSubstituteInfo());
    SNode attributeConcept = provider.getRoleAttribute();
    Class attributeKind = provider.getRoleAttributeClass();
    if (attributeConcept != null) {
      EditorManager manager = EditorManager.getInstanceFromContext(editorContext);
      return manager.createNodeRoleAttributeCell(editorContext, attributeConcept, attributeKind, editorCell);
    } else
    return editorCell;
  }
  private EditorCell createConstant_jpw21l_b0a(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "as");
    editorCell.setCellId("Constant_jpw21l_b0a");
    Style style = new StyleImpl();
    {
      Style styleToPut;
      styleToPut = new StyleImpl();
      BaseLanguageStyle_StyleSheet.apply_KeyWord(styleToPut, editorCell);
      style.putAll(styleToPut, 0);
    }
    editorCell.getStyle().putAll(style);
    editorCell.setDefaultText("");
    return editorCell;
  }
  private EditorCell createConstant_jpw21l_c0a(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "");
    editorCell.setCellId("Constant_jpw21l_c0a");
    editorCell.setDefaultText("");
    return editorCell;
  }
  private EditorCell createRefNode_jpw21l_d0a(EditorContext editorContext, SNode node) {
    CellProviderWithRole provider = new RefNodeCellProvider(node, editorContext);
    provider.setRole("type");
    provider.setNoTargetText("<no type>");
    EditorCell editorCell;
    editorCell = provider.createEditorCell(editorContext);
    if (editorCell.getRole() == null) {
      editorCell.setRole("type");
    }
    editorCell.setSubstituteInfo(provider.createDefaultSubstituteInfo());
    SNode attributeConcept = provider.getRoleAttribute();
    Class attributeKind = provider.getRoleAttributeClass();
    if (attributeConcept != null) {
      EditorManager manager = EditorManager.getInstanceFromContext(editorContext);
      return manager.createNodeRoleAttributeCell(editorContext, attributeConcept, attributeKind, editorCell);
    } else
    return editorCell;
  }
  private EditorCell createConstant_jpw21l_e0a(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "@");
    editorCell.setCellId("Constant_jpw21l_e0a");
    Style style = new StyleImpl();
    {
      Style styleToPut;
      styleToPut = new StyleImpl();
      BaseLanguageStyle_StyleSheet.apply_Operator(styleToPut, editorCell);
      style.putAll(styleToPut, 0);
    }
    editorCell.getStyle().putAll(style);
    editorCell.setDefaultText("");
    return editorCell;
  }
  private EditorCell createConstant_jpw21l_f0a(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "[");
    editorCell.setCellId("Constant_jpw21l_f0a");
    Style style = new StyleImpl();
    {
      Style styleToPut;
      styleToPut = new StyleImpl();
      BaseLanguageStyle_StyleSheet.apply_LeftBracket(styleToPut, editorCell);
      style.putAll(styleToPut, 0);
    }
    editorCell.getStyle().putAll(style);
    editorCell.setDefaultText("");
    return editorCell;
  }
  private EditorCell createProperty_jpw21l_g0a(EditorContext editorContext, SNode node) {
    CellProviderWithRole provider = new PropertyCellProvider(node, editorContext);
    provider.setRole("index");
    provider.setNoTargetText("<no index>");
    EditorCell editorCell;
    editorCell = provider.createEditorCell(editorContext);
    editorCell.setCellId("property_index");
    editorCell.setSubstituteInfo(provider.createDefaultSubstituteInfo());
    SNode attributeConcept = provider.getRoleAttribute();
    Class attributeKind = provider.getRoleAttributeClass();
    if (attributeConcept != null) {
      EditorManager manager = EditorManager.getInstanceFromContext(editorContext);
      return manager.createNodeRoleAttributeCell(editorContext, attributeConcept, attributeKind, editorCell);
    } else
    return editorCell;
  }
  private EditorCell createConstant_jpw21l_h0a(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "]");
    editorCell.setCellId("Constant_jpw21l_h0a");
    Style style = new StyleImpl();
    {
      Style styleToPut;
      styleToPut = new StyleImpl();
      BaseLanguageStyle_StyleSheet.apply_RightBracket(styleToPut, editorCell);
      style.putAll(styleToPut, 0);
    }
    editorCell.getStyle().putAll(style);
    editorCell.setDefaultText("");
    return editorCell;
  }
  private EditorCell createConstant_jpw21l_i0a(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "capture ");
    editorCell.setCellId("Constant_jpw21l_i0a");
    Style style = new StyleImpl();
    {
      Style styleToPut;
      styleToPut = new StyleImpl();
      BaseLanguageStyle_StyleSheet.apply_KeyWord(styleToPut, editorCell);
      style.putAll(styleToPut, 0);
    }
    editorCell.getStyle().putAll(style);
    editorCell.setDefaultText("");
    return editorCell;
  }
  private static boolean renderingCondition_jpw21l_a8a0(SNode node, EditorContext editorContext) {
    return SPropertyOperations.getInteger(node, MetaAdapterFactory.getProperty(0xecee57946eda4903L, 0x830384b2f078edf4L, 0x619dcea887ca8bb4L, 0x1fe32db4944572dbL, "index")) > 0;
  }
  private EditorCell createProperty_jpw21l_j0a(EditorContext editorContext, SNode node) {
    CellProviderWithRole provider = new PropertyCellProvider(node, editorContext);
    provider.setRole("pattern");
    provider.setNoTargetText("<no pattern>");
    EditorCell editorCell;
    editorCell = provider.createEditorCell(editorContext);
    editorCell.setCellId("property_pattern");
    Style style = new StyleImpl();
    {
      Style styleToPut;
      styleToPut = StyleRegistry.getInstance().getStyle("TODO");
      style.putAll(styleToPut, 0);
    }
    editorCell.getStyle().putAll(style);
    editorCell.setSubstituteInfo(provider.createDefaultSubstituteInfo());
    SNode attributeConcept = provider.getRoleAttribute();
    Class attributeKind = provider.getRoleAttributeClass();
    if (attributeConcept != null) {
      EditorManager manager = EditorManager.getInstanceFromContext(editorContext);
      return manager.createNodeRoleAttributeCell(editorContext, attributeConcept, attributeKind, editorCell);
    } else
    return editorCell;
  }
  private static boolean renderingCondition_jpw21l_a9a0(SNode node, EditorContext editorContext) {
    return SPropertyOperations.getInteger(node, MetaAdapterFactory.getProperty(0xecee57946eda4903L, 0x830384b2f078edf4L, 0x619dcea887ca8bb4L, 0x1fe32db4944572dbL, "index")) > 0;
  }
  private EditorCell createConstant_jpw21l_b0(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "converter");
    editorCell.setCellId("Constant_jpw21l_b0");
    editorCell.setDefaultText("");
    return editorCell;
  }
  private static boolean renderingCondition_jpw21l_a1a(SNode node, EditorContext editorContext) {
    return !(Member_Behavior.call_isStringType_5450232044631307524(node));
  }
  private EditorCell createRefNode_jpw21l_c0(EditorContext editorContext, SNode node) {
    CellProviderWithRole provider = new RefNodeCellProvider(node, editorContext);
    provider.setRole("converter");
    provider.setNoTargetText("<no converter>");
    EditorCell editorCell;
    editorCell = provider.createEditorCell(editorContext);
    if (editorCell.getRole() == null) {
      editorCell.setRole("converter");
    }
    editorCell.setSubstituteInfo(provider.createDefaultSubstituteInfo());
    SNode attributeConcept = provider.getRoleAttribute();
    Class attributeKind = provider.getRoleAttributeClass();
    if (attributeConcept != null) {
      EditorManager manager = EditorManager.getInstanceFromContext(editorContext);
      return manager.createNodeRoleAttributeCell(editorContext, attributeConcept, attributeKind, editorCell);
    } else
    return editorCell;
  }
  private static boolean renderingCondition_jpw21l_a2a(SNode node, EditorContext editorContext) {
    return !(Member_Behavior.call_isStringType_5450232044631307524(node));
  }
  private EditorCell createCollection_jpw21l_a_0(EditorContext editorContext, SNode node) {
    EditorCell_Collection editorCell = EditorCell_Collection.createVertical(editorContext, node);
    editorCell.setCellId("Collection_jpw21l_a_0");
    editorCell.setBig(true);
    if (renderingCondition_jpw21l_a0a(node, editorContext)) {
      editorCell.addEditorCell(this.createConstant_jpw21l_a0(editorContext, node));
    }
    if (renderingCondition_jpw21l_a1a_0(node, editorContext)) {
      editorCell.addEditorCell(this.createRefNode_jpw21l_b0(editorContext, node));
    }
    return editorCell;
  }
  private EditorCell createConstant_jpw21l_a0(EditorContext editorContext, SNode node) {
    EditorCell_Constant editorCell = new EditorCell_Constant(editorContext, node, "initializer");
    editorCell.setCellId("Constant_jpw21l_a0");
    editorCell.setDefaultText("");
    return editorCell;
  }
  private static boolean renderingCondition_jpw21l_a0a(SNode node, EditorContext editorContext) {
    return !((SPropertyOperations.getInteger(node, MetaAdapterFactory.getProperty(0xecee57946eda4903L, 0x830384b2f078edf4L, 0x619dcea887ca8bb4L, 0x1fe32db4944572dbL, "index")) > 0));
  }
  private EditorCell createRefNode_jpw21l_b0(EditorContext editorContext, SNode node) {
    CellProviderWithRole provider = new RefNodeCellProvider(node, editorContext);
    provider.setRole("initializer");
    provider.setNoTargetText("<no initializer>");
    EditorCell editorCell;
    editorCell = provider.createEditorCell(editorContext);
    if (editorCell.getRole() == null) {
      editorCell.setRole("initializer");
    }
    editorCell.setSubstituteInfo(provider.createDefaultSubstituteInfo());
    SNode attributeConcept = provider.getRoleAttribute();
    Class attributeKind = provider.getRoleAttributeClass();
    if (attributeConcept != null) {
      EditorManager manager = EditorManager.getInstanceFromContext(editorContext);
      return manager.createNodeRoleAttributeCell(editorContext, attributeConcept, attributeKind, editorCell);
    } else
    return editorCell;
  }
  private static boolean renderingCondition_jpw21l_a1a_0(SNode node, EditorContext editorContext) {
    return !((SPropertyOperations.getInteger(node, MetaAdapterFactory.getProperty(0xecee57946eda4903L, 0x830384b2f078edf4L, 0x619dcea887ca8bb4L, 0x1fe32db4944572dbL, "index")) > 0));
  }
}
