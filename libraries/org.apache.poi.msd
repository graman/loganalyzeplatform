<?xml version="1.0" encoding="UTF-8"?>
<solution name="org.apache.poi" uuid="f2d18ae0-3a8e-433b-9851-e50a45f80e9b" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
    <modelRoot contentPath="${module}" type="java_classes">
      <sourceRoot location="models" />
    </modelRoot>
    <modelRoot contentPath="${module}/models/poi-3.11.jar!" type="java_classes" />
  </models>
  <stubModelEntries>
    <stubModelEntry path="${module}/models/poi-3.11.jar" />
    <stubModelEntry path="${module}/models/poi-ooxml-3.11-beta3.jar" />
    <stubModelEntry path="${module}/models/poi-ooxml-schemas-3.11-beta3.jar" />
    <stubModelEntry path="${module}/models/xmlbeans-2.6.0.jar" />
    <stubModelEntry path="${module}/models/stax-api-1.0.1.jar" />
  </stubModelEntries>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">6354ebe7-c22a-4a0f-ac54-50b52ab9b065(JDK)</dependency>
  </dependencies>
  <languageVersions>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" fqName="jetbrains.mps.lang.core" version="1" />
  </languageVersions>
</solution>

